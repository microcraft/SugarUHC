 package com.microcraftmc.uhc.listeners;
 
 import com.microcraftmc.uhc.Settings;
 import org.bukkit.event.Listener;
 import org.bukkit.event.entity.FoodLevelChangeEvent;
 import org.bukkit.event.EventHandler;
 
 public class PlayerHunger implements Listener
 {
   @EventHandler
   public void onPlayerHunger(FoodLevelChangeEvent event)
   {
     if (event.isCancelled()) { return;
     }
     if ((!Settings.GameInProgress) || (!Settings.PVPEnabled)) {
       event.setFoodLevel(20);
     }
   }
 }


