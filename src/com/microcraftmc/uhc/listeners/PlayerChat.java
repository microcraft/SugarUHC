package com.microcraftmc.uhc.listeners;

 import com.microcraftmc.uhc.config.ConfigManager;
 import com.microcraftmc.uhc.handlers.PlayerHandler;
 import org.bukkit.Bukkit;
 import org.bukkit.ChatColor;
 import org.bukkit.entity.Player;
 import org.bukkit.event.EventHandler;
 import org.bukkit.event.Listener;
 import org.bukkit.event.player.AsyncPlayerChatEvent;

 public class PlayerChat implements Listener
 {
   @EventHandler
   public void onPlayerChat(AsyncPlayerChatEvent e)
   {
        if (PlayerHandler.getInstance().isUHCHost(e.getPlayer())) {
            e.setFormat(ChatColor.GRAY + "[" + ChatColor.DARK_RED + "UHC-Host" + ChatColor.GRAY + "]" + ChatColor.RESET + " " + e.getPlayer().getName() + " " + ">" + " " + e.getMessage());
        }

        if (PlayerHandler.getInstance().isUHCMod(e.getPlayer())) {
            e.setFormat(ChatColor.GRAY + "[" + ChatColor.AQUA + "UHC-Mod" + ChatColor.GRAY + "]" + ChatColor.RESET + " " + e.getPlayer().getName() + " " + ">" + " " + e.getMessage());
        }
   
         if (!PlayerHandler.getInstance().isUHCHost(e.getPlayer())) {
            if (!PlayerHandler.getInstance().isUHCMod(e.getPlayer())) {
                if (!e.getPlayer().isOp()) {
            	    e.setFormat(ChatColor.DARK_GRAY + e.getPlayer().getName() + " " + ChatColor.WHITE +">" + " " + ChatColor.GRAY + e.getMessage());
                }
            }

        }
   
}

 }