 package com.microcraftmc.uhc.listeners;

 import com.microcraftmc.uhc.config.ConfigManager;
 import com.microcraftmc.uhc.config.LobbyManager;
 import com.microcraftmc.uhc.handlers.PlayerHandler;
 import com.microcraftmc.uhc.scoreboard.LobbyScoreboardManager;
 import com.microcraftmc.uhc.scoreboard.ScoreboardHandler;
 import com.microcraftmc.uhc.Settings;
 import com.microcraftmc.titleapi.TitleAPI;

 import org.bukkit.Bukkit;
 import org.bukkit.ChatColor;
 import org.bukkit.GameMode;
 import org.bukkit.Location;
 import org.bukkit.entity.Player;
 import org.bukkit.event.EventHandler;
 import org.bukkit.event.player.PlayerJoinEvent;
 import org.bukkit.event.Listener;

 public class PlayerJoin implements Listener
 {
   @EventHandler
   public void onPlayerJoin(PlayerJoinEvent event)
   {
        Player player = event.getPlayer();

        player.teleport(LobbyManager.getInstance().getLobbySpawn());

        player.setGameMode(GameMode.SURVIVAL);
        player.getInventory().clear();

        //add the player to our players list
       PlayerHandler.getInstance().addPlayer(player);

        if (!ConfigManager.getInstance().isDebugEnabled()) {
            event.setJoinMessage(ChatColor.YELLOW + player.getDisplayName() + ChatColor.AQUA + " has joined the game (" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers() + ")");
        } else {
            event.setJoinMessage("Welcome " + ChatColor.YELLOW + player.getDisplayName() + ChatColor.AQUA + ". SugarUHC is currently disabled on this server for maintenance, sorry for the inconvenience.");
        }

        if(!Settings.GameInProgress) {
            //should we enable the lobby scoreboard?
            if (ConfigManager.getInstance().getScoreboardsConfig().getBoolean("lobby-scoreboard.enabled")) {
                LobbyScoreboardManager.getInstance().setupScoreboardLobby(player);
                LobbyScoreboardManager.getInstance().updateScoreboardLobby();
            }

            if (ConfigManager.getInstance().getScoreboardsConfig().getBoolean("tab-title.enabled")) {
                TitleAPI.sendTabTitle(player, ConfigManager.getInstance().getScoreboardsConfig().getString("tab-title.header"),
                        ConfigManager.getInstance().getScoreboardsConfig().getString("tab-title.footer"));
            }

            if (ConfigManager.getInstance().getScoreboardsConfig().getBoolean("title.enabled")) {
                TitleAPI.sendTitle(player,
                        ConfigManager.getInstance().getScoreboardsConfig().getInt("title.fade.in"),
                        ConfigManager.getInstance().getScoreboardsConfig().getInt("title.fade.stay"),
                        ConfigManager.getInstance().getScoreboardsConfig().getInt("title.fade.out"),
                        ConfigManager.getInstance().getScoreboardsConfig().getString("title.title"),
                        ConfigManager.getInstance().getScoreboardsConfig().getString("title.subtitle"));
            }

        }
     
        if (Settings.ShowHealthInTab) {
            ScoreboardHandler.activateScoreboard(player);
        }
   }

 }


