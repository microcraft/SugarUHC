package com.microcraftmc.uhc.listeners;

import com.microcraftmc.uhc.Settings;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;

public class BlockBreak implements Listener
{
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event)
    {
        if (event.isCancelled()) {
            return;
        }
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if (!Settings.GameInProgress) {
            event.setCancelled(true);
            return;
        }

        /*
         *
         * do we have speed mode enabled?
         * if we do, give the player an item so they don't have to use a furnace
         *
         */
        if (Settings.SpeedMode) {
            if (block.getType() == Material.IRON_ORE) {

                event.setCancelled(true);
                block.setType(Material.AIR);
                ItemStack IronIngot = new ItemStack(Material.IRON_INGOT, 1);
                player.getInventory().addItem(new ItemStack[] { IronIngot });

            } else if (block.getType() == Material.GOLD_ORE) {

                event.setCancelled(true);
                block.setType(Material.AIR);
                ItemStack GoldIngot = new ItemStack(Material.GOLD_INGOT, 1);
                player.getInventory().addItem(new ItemStack[] { GoldIngot });

            } else if (block.getType() == Material.COAL_ORE) {

                event.setCancelled(true);
                block.setType(Material.AIR);
                ItemStack Coal = new ItemStack(Material.COAL, 1);
                player.getInventory().addItem(new ItemStack[] { Coal });

            } else if (block.getType() == Material.DIAMOND_ORE) {

                event.setCancelled(true);
                block.setType(Material.AIR);
                ItemStack Diamond = new ItemStack(Material.DIAMOND, 1);
                player.getInventory().addItem(new ItemStack[] { Diamond });

            }
        }
    }
}


