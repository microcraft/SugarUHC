 package com.microcraftmc.uhc.listeners;
 
 import com.microcraftmc.uhc.Settings;
 import org.bukkit.entity.Player;
 import org.bukkit.event.entity.EntityDamageEvent;
 import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
 
 public class PlayerDamage implements org.bukkit.event.Listener
 {
   @org.bukkit.event.EventHandler
   public void onPlayerDamage(EntityDamageEvent event)
   {
     if (event.isCancelled()) { return;
     }
     if ((event.getEntity() instanceof Player))
     {
       Player player = (Player)event.getEntity();
       if (!Settings.GameInProgress) {
         event.setCancelled(true);
        return;
       }
       
       if (event.getCause() == DamageCause.FALL) {
         Integer safeTime = Integer.valueOf(Settings.GameTime.intValue() * 60 - 5);
         if (Settings.TimeLeft.intValue() > safeTime.intValue()) {
           event.setCancelled(true);
           return;
         }
       }
       
       if (!Settings.ShowHealthInTab) {}
     }
   }
 }


