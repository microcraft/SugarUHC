 package com.microcraftmc.uhc.listeners;
 
 import org.bukkit.ChatColor;
 import org.bukkit.event.EventHandler;
 import org.bukkit.event.Listener;
 import org.bukkit.event.player.PlayerLoginEvent;
 import org.bukkit.event.player.PlayerLoginEvent.Result;
 import com.microcraftmc.uhc.Settings;
 
 public class PlayerLogin implements Listener
 {
   @EventHandler
   public void onPlayerLogin(PlayerLoginEvent event)
   {
     org.bukkit.entity.Player player = event.getPlayer();
     
     if ((event.getResult() == Result.ALLOWED) &&
       (Settings.GameInProgress)) {
       event.setResult(Result.KICK_FULL);
       event.setKickMessage(ChatColor.RED + "Game is currently in progress");
     }
   }
 }


