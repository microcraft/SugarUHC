 package com.microcraftmc.uhc.listeners;
 
 import org.bukkit.entity.Player;
 import org.bukkit.event.Listener;
 import org.bukkit.event.entity.EntityRegainHealthEvent;
 import org.bukkit.event.EventHandler;
 
 public class PlayerHealth implements Listener
 {
   @EventHandler
   public void onPlayerHealth(EntityRegainHealthEvent event)
   {
     if (event.isCancelled()) { return;
     }
     if ((event.getEntity() instanceof Player)) {
       Player player = (Player)event.getEntity();
       if (event.getRegainReason() != EntityRegainHealthEvent.RegainReason.MAGIC_REGEN) {
         event.setCancelled(true);
         return;
       }
     }
   }
 }


