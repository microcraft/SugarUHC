 package com.microcraftmc.uhc.listeners;
 

 import com.microcraftmc.uhc.handlers.GameHandler;
 import com.microcraftmc.uhc.Settings;

 import org.bukkit.ChatColor;
 import org.bukkit.GameMode;
 import org.bukkit.entity.Player;
 import org.bukkit.event.entity.PlayerDeathEvent;
 import org.bukkit.event.Listener;
 import org.bukkit.event.EventHandler;
 
 public class PlayerDeath implements Listener
 {
   @EventHandler
   public void onPlayerDeath(PlayerDeathEvent event)
   {
    Player player = event.getEntity();
     
     if (Settings.Players.contains(player.getUniqueId())) {
       Settings.Players.remove(player.getUniqueId());
       GameHandler.checkForWinner();
       
       event.setDeathMessage(ChatColor.RED + event.getDeathMessage());
       
      if (Settings.SpectatingEnabled) {
         player.setGameMode(GameMode.SPECTATOR);
       } else {
         player.kickPlayer(ChatColor.RED + "" + ChatColor.BOLD + "You Died! " + ChatColor.RESET + "Thank you for playing Ultra Hardcore.");
       }
     } else {
       event.setDeathMessage(null);
     }
     
     if (Settings.SpectatingEnabled) {
       GameHandler.checkForWinner();
     }
   }
 }


