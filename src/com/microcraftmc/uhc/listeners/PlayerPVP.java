 package com.microcraftmc.uhc.listeners;
 
 import com.microcraftmc.uhc.Settings;
 import org.bukkit.entity.Player;
 import org.bukkit.event.Listener;
 import org.bukkit.event.entity.EntityDamageByEntityEvent;
 
 public class PlayerPVP implements Listener
 {
   @org.bukkit.event.EventHandler
   public void onPlayerPVP(EntityDamageByEntityEvent event)
   {
     if (event.isCancelled()) { return;
     }
     if (((event.getEntity() instanceof Player)) && ((event.getDamager() instanceof Player)) && (
       (!Settings.GameInProgress) || (!Settings.PVPEnabled))) {
       event.setCancelled(true);
     }
   }
 }


