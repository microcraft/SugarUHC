 package com.microcraftmc.uhc.listeners;
 
 import com.microcraftmc.uhc.Settings;

 import org.bukkit.ChatColor;
 import org.bukkit.event.Listener;
 import org.bukkit.event.server.ServerListPingEvent;
 import org.bukkit.event.EventHandler;
 
 public class ServerPing implements Listener
 {
   @EventHandler
   public void onServerPing(ServerListPingEvent event)
   {
     if (Settings.GameInProgress) {
       event.setMotd(ChatColor.GOLD + "Game is currently in progress (" + Settings.Players.size() + "/" + org.bukkit.Bukkit.getMaxPlayers() + ")");
     }
   }
 }


