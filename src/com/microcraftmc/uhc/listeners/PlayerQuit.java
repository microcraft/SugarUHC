 package com.microcraftmc.uhc.listeners;
 
 import com.microcraftmc.uhc.Settings;

 import org.bukkit.entity.Player;
 import org.bukkit.event.Listener;
 import org.bukkit.event.player.PlayerQuitEvent;
 
 public class PlayerQuit implements Listener
 {
   @org.bukkit.event.EventHandler
   public void onPlayerQuit(PlayerQuitEvent event)
   {
     Player player = event.getPlayer();
     
     if (Settings.Players.contains(player.getUniqueId())) {
       Settings.Players.remove(player.getUniqueId());
       event.setQuitMessage(null);
       
       com.microcraftmc.uhc.handlers.GameHandler.checkForWinner();
     } else if (!Settings.GameInProgress) {
      event.setQuitMessage(null);
     }
     else {
       event.setQuitMessage(null);
     }
   }
 }


