 package com.microcraftmc.uhc.handlers;
 
 import com.microcraftmc.uhc.Settings;
 import com.microcraftmc.uhc.SugarUHC;

 import java.util.ArrayList;
 import java.util.Arrays;
 import java.util.Iterator;
 import java.util.List;

 import com.microcraftmc.uhc.config.ConfigManager;
 import com.microcraftmc.uhc.config.MessageManager;
 import com.microcraftmc.uhc.config.MessageManager.PrefixType;
 import com.microcraftmc.uhc.scoreboard.GameScoreboardManager;
 import com.microcraftmc.uhc.utils.ScatterUtil;
 import org.bukkit.Bukkit;
 import org.bukkit.ChatColor;
 import org.bukkit.WorldBorder;
 import org.bukkit.entity.Player;
 import org.bukkit.potion.PotionEffect;
 import org.bukkit.potion.PotionEffectType;
 import org.bukkit.scheduler.BukkitRunnable;
 import com.microcraftmc.titleapi.TitleAPI;

 
 public class GameHandler
 {
     private static GameHandler instance = new GameHandler();
     private boolean countdownRunning;
     private List<String> rulesList = ConfigManager.getInstance().getRulesConfig().getStringList("default-msgs.rules");

   public GameHandler() {

   }

   public static GameHandler getInstance() {
       return instance;
   }



   public void StartGame()
   {
       if ((Settings.GameInProgress) || (Settings.GameOver) || (Settings.GameInMeetup)) return;

       Bukkit.broadcastMessage(ChatColor.GOLD + "Starting game...");

        for (Player player : PlayerHandler.getInstance().getPlayers()) {
            Settings.Players.add(player.getUniqueId());


            for (PotionEffect effect : player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }
       
 
            PotionEffect potionEffect = new PotionEffect(PotionEffectType.BLINDNESS, 99999, 1);
            player.addPotionEffect(potionEffect);
            potionEffect = new PotionEffect(PotionEffectType.SLOW, 99999, 10);
            player.addPotionEffect(potionEffect);
            potionEffect = new PotionEffect(PotionEffectType.JUMP, 99999, 200);
            player.addPotionEffect(potionEffect);
       
 
            player.setHealth(20.0D);
            player.setFoodLevel(20);
       
 
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
            player.getInventory().setHelmet(new org.bukkit.inventory.ItemStack(org.bukkit.Material.AIR));
            player.getInventory().setChestplate(new org.bukkit.inventory.ItemStack(org.bukkit.Material.AIR));
            player.getInventory().setLeggings(new org.bukkit.inventory.ItemStack(org.bukkit.Material.AIR));
            player.getInventory().setBoots(new org.bukkit.inventory.ItemStack(org.bukkit.Material.AIR));
       
 
            player.setExp(0.0F);
            player.setLevel(0);
            player.setFireTicks(0);
            GameScoreboardManager.getInstance().setupScoreboardGame(player);
     }
     
        org.bukkit.World world = Bukkit.getWorld(Settings.WorldName);
        world.setDifficulty(org.bukkit.Difficulty.HARD);
        WorldBorder worldBorder = Bukkit.getWorld(Settings.WorldName).getWorldBorder();
        worldBorder.setCenter(0.0D, 0.0D);
        worldBorder.setSize(Settings.MapRadius.intValue() * 2);
     
        Bukkit.broadcastMessage(ChatColor.GOLD + "Scattering players...");

        Integer i = Integer.valueOf(0);
        Integer localInteger1; Integer localInteger2; for (Iterator i$ = PlayerHandler.getInstance().getPlayers().iterator(); i$.hasNext();
         

        localInteger2 = i = i + 1)
        {
            final Player player = (Player)i$.next();
            BukkitRunnable scatter = new BukkitRunnable()
        {
            public void run() {
                com.microcraftmc.uhc.utils.ScatterUtil.scatterPlayerRandom(player, Settings.MapRadius);
            }
         
        };
      scatter.runTaskLater(SugarUHC.getPlugin(), i * 2L);
       localInteger1 = i;
     }
     
     i = i + 5;
     BukkitRunnable finalRunnable = new BukkitRunnable()
     {
       public void run() {
         for (Iterator i$ = PlayerHandler.getInstance().getPlayers().iterator(); i$.hasNext();) { Player player = (Player)i$.next();
           for (PotionEffect effect : player.getActivePotionEffects())
             player.removePotionEffect(effect.getType());
         }
         Player player;
         org.bukkit.World world = Bukkit.getWorld(Settings.WorldName);
        if (world != null) world.setFullTime(0L);
         Bukkit.broadcastMessage(ChatColor.GOLD + "Game has started");
         Settings.GameInProgress = true;
         if (Settings.PVPTime > 0) {
           Bukkit.broadcastMessage(ChatColor.GOLD + "PVP will be enabled in " + Settings.PVPTime + " minutes");
           GameHandler.StartPVPCountdown();
         } else {
           Settings.PVPEnabled = true;
         }
         GameHandler.StartMeetUpCountdown();
       }
       
     };
     finalRunnable.runTaskLater(SugarUHC.getPlugin(), i * 3L);
     com.microcraftmc.uhc.utils.WorldUtil.clearSpawn(Settings.WorldName);
     //GameScoreboardManager.getInstance().updateScoreboardGame();
   }
   
   public static void StartMeetUp() {
     if ((!Settings.GameInProgress) || (Settings.GameOver) || (Settings.GameInMeetup)) return;
     Bukkit.broadcastMessage(ChatColor.GOLD + "Starting meet up...");
     
     for (Player player : Bukkit.getOnlinePlayers())
     {
       PotionEffect potionEffect = new PotionEffect(PotionEffectType.BLINDNESS, 99999, 1);
       player.addPotionEffect(potionEffect);
       potionEffect = new PotionEffect(PotionEffectType.SLOW, 99999, 10);
       player.addPotionEffect(potionEffect);
       potionEffect = new PotionEffect(PotionEffectType.JUMP, 99999, 200);
      player.addPotionEffect(potionEffect);
     }
     
     Integer i = Integer.valueOf(0);
     Integer localInteger1; Integer localInteger2; for (Iterator i$ = Bukkit.getOnlinePlayers().iterator(); i$.hasNext();

 
         localInteger2 = i = Integer.valueOf(i.intValue() + 1))
     {
       final Player player = (Player)i$.next();
       BukkitRunnable scatter = new BukkitRunnable()
       {
         public void run() {
           com.microcraftmc.uhc.utils.ScatterUtil.scatterPlayerRandom(player, Integer.valueOf(10));
         }
         
       };
       scatter.runTaskLater(SugarUHC.getPlugin(), i.intValue() * 2L);
       localInteger1 = i;
     }
     
     i = Integer.valueOf(i.intValue() + 5);
     BukkitRunnable finalRunnable = new BukkitRunnable()
     {
       public void run() {
         Settings.GameInProgress = true;
         for (Player player : Bukkit.getOnlinePlayers()) {
           player.removePotionEffect(PotionEffectType.BLINDNESS);
           player.removePotionEffect(PotionEffectType.SLOW);
          player.removePotionEffect(PotionEffectType.JUMP);
         }
         Bukkit.broadcastMessage(ChatColor.GOLD + "Meet up has started");
         Settings.PVPEnabled = true;
         
         Settings.GameInMeetup = true;
         WorldBorder worldBorder = Bukkit.getWorld(Settings.WorldName).getWorldBorder();
         worldBorder.setCenter(0.0D, 0.0D);
         worldBorder.setSize(200.0D);
       }
       
     };
     finalRunnable.runTaskLater(SugarUHC.getPlugin(), i.intValue() * 3L);
     com.microcraftmc.uhc.utils.WorldUtil.clearSpawn(Settings.WorldName);
   }

   public void StartGameCountdown() {
     if ((Settings.GameInProgress) || (Settings.GameOver)) { return;
     }
        final Integer[] countdown = { 60 };
        final boolean GameInProgress = Settings.GameInProgress;
        final boolean GameOver = Settings.GameOver;
        final Integer MinPlayers = Settings.MinPlayers;

     BukkitRunnable GameStartCountdown = new BukkitRunnable()
     {
       public void run() {
         if ((GameInProgress) || (GameOver)) return;
         if (!ConfigManager.getInstance().isDebugEnabled()) {
             if (PlayerHandler.getInstance().getPlayers().size() < MinPlayers) {
                 countdown[0] = 60;
                 return;
             }
         } else {
             countdown[0] = 60;
         }
           if (countdown[0] > 0) {

                    if (countdown[0] <= 10) {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "Game starting in " + countdown[0] + " seconds");
                    } else if (countdown[0] == 15) {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "Game starting in " + countdown[0] + " seconds");
                    } else if (countdown[0] == 30) {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "Game starting in " + countdown[0] + " seconds");
                    } else if (countdown[0] == 45) {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "Game starting in " + countdown[0] + " seconds");
                    } else if (countdown[0] == 60) {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "Game starting in " + countdown[0] + " seconds");
                    }

               countdown[0] = countdown[0] - 1;
           } else {
               StartGame();
               cancel();
           }

           for (Player player : PlayerHandler.getInstance().getPlayers()) {
               player.setLevel(countdown[0]);
           }
       }

     };
    GameStartCountdown.runTaskTimer(SugarUHC.getPlugin(), 0L, 20L);
   }


     int rulesCount = rulesList.size();
     public void PreGameRules() {

         try {

             MessageManager.getInstance().broadcastMessage(PrefixType.INFO, ConfigManager.getInstance().getRulesConfig().getString("default-msgs.rules-start"));
             Thread.sleep(1000);

             if (rulesCount > 0) {
                 for (String rule : rulesList) {
                     MessageManager.getInstance().broadcastMessage(PrefixType.INFO, rule);
                     Thread.sleep(2000);
                     rulesCount--;
                 }
             } else {

                 MessageManager.getInstance().broadcastMessage(PrefixType.INFO, ChatColor.GOLD + "Good luck, and enjoy!");
                 Thread.sleep(1000);
                 StartGameCountdown();

             }

         } catch (Exception e) {
             e.printStackTrace();
         }

     }

     public void StartRulesCountdown() {


        BukkitRunnable RulesCountdown = new BukkitRunnable() {
            public void run() {

                try {

                    MessageManager.getInstance().broadcastMessage(PrefixType.INFO, ConfigManager.getInstance().getRulesConfig().getString("default-msgs.rules-start"));
                    Thread.sleep(2000);

                    for (String rule : rulesList) {
                            MessageManager.getInstance().broadcastMessage(PrefixType.INFO, rule);
                            Thread.sleep(3000);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                MessageManager.getInstance().broadcastMessage(PrefixType.INFO, ChatColor.GOLD + "Good luck, and enjoy!");
                StartGameCountdown();
                cancel();
            }
        };

        RulesCountdown.runTask(SugarUHC.getPlugin());

     }

   public static void StartPVPCountdown() {
     if (Settings.PVPEnabled) return;
     final Integer[] countdown = { Integer.valueOf(Settings.PVPTime.intValue() * 60) };
     BukkitRunnable PVPCountdown = new BukkitRunnable()
     {
       public void run() {
         if ((Settings.GameInProgress) || (Settings.GameOver)) return;
        if (countdown[0].intValue() == 0) {
           Bukkit.broadcastMessage(ChatColor.GOLD + "PVP has been enabled");
          Settings.PVPEnabled = true;
           cancel();
         } else if (countdown[0].intValue() <= 10) {
           Bukkit.broadcastMessage(ChatColor.GOLD + "PVP will be enabled in " + countdown[0] + " seconds");
         }
         countdown[0] = Integer.valueOf(countdown[0].intValue() - 1);
       }
       
     };
    PVPCountdown.runTaskTimer(SugarUHC.getPlugin(), 0L, 20L);
   }
   
   public static void StartMeetUpCountdown() {
     if ((!Settings.GameInProgress) || (Settings.GameInMeetup)) { return;
     }
     Settings.TimeLeft = Integer.valueOf(Settings.GameTime.intValue() * 60);
     final boolean GameInProgress = Settings.GameInProgress;
     final boolean GameInMeetup = Settings.GameInMeetup;
     
     BukkitRunnable DrawCountdown = new BukkitRunnable()
     {
       public void run() {
         if ((!GameInProgress) || (GameInMeetup)) return;
         if (Settings.TimeLeft.intValue() == 0) {
           GameHandler.StartMeetUp();
           cancel();
         } else if ((Settings.TimeLeft.intValue() <= 10) || (Settings.TimeLeft.intValue() == 30) || (Settings.TimeLeft.intValue() == 60)) {
           Bukkit.broadcastMessage(ChatColor.GOLD + "Meet up in " + Settings.TimeLeft + " seconds");
         }
         Settings.TimeLeft = Integer.valueOf(Settings.TimeLeft.intValue() - 1);
         String header; String footer; if (Settings.ShowTimeInTab) {
           header = "" + ChatColor.GOLD + ChatColor.BOLD + "Ultra Hardcore";
           footer = ChatColor.GREEN + com.microcraftmc.uhc.utils.TimeUtil.timeConversion(Settings.TimeLeft.intValue());
           
           for (Player player : Bukkit.getOnlinePlayers()) {
             TitleAPI.sendTabTitle(player, header, footer);
           }
           
         }
       }
     };
     DrawCountdown.runTaskTimerAsynchronously(SugarUHC.getPlugin(), 0L, 20L);
   }
   
   public static void StartDrawCountdown() {
     if (!Settings.GameInProgress) { return;
     }
     Settings.TimeLeft = Integer.valueOf(Settings.GameTime.intValue() * 60);
     BukkitRunnable DrawCountdown = new BukkitRunnable()
     {
       public void run() {
         if (!Settings.GameInProgress) return;
         if (Settings.TimeLeft.intValue() == 0) {
           GameHandler.EndGameNoWinner();
          cancel();
         } else if (Settings.TimeLeft.intValue() <= 10) {
           Bukkit.broadcastMessage(ChatColor.GOLD + "Game draw in " + Settings.TimeLeft + " seconds");
         }
         Settings.TimeLeft = Integer.valueOf(Settings.TimeLeft.intValue() - 1);
       }
       
     };
     DrawCountdown.runTaskTimer(SugarUHC.getPlugin(), 0L, 20L);
   }
   
   public static void StartServerRestartCountdown() {
     if (Settings.GameInProgress) { return;
     }
     final Integer[] countdown = { Integer.valueOf(10) };
     BukkitRunnable DrawCountdown = new BukkitRunnable()
     {
       public void run() {
         if (countdown[0].intValue() == -1) {
           Bukkit.shutdown();
           cancel();
         } else if (countdown[0].intValue() == 0) {
           Bukkit.broadcastMessage(ChatColor.GOLD + "Restarting server...");
         } else if ((countdown[0].intValue() == 10) || (countdown[0].intValue() <= 5)) {
           Bukkit.broadcastMessage(ChatColor.GOLD + "Server restart in " + countdown[0] + " seconds");
         }
         countdown[0] = Integer.valueOf(countdown[0].intValue() - 1);
       }
       
     };
     DrawCountdown.runTaskTimer(SugarUHC.getPlugin(), 0L, 20L);
   }
   
   public static void checkForWinner() {
     if (Settings.Players.size() == 1) {
       Player winner = Bukkit.getPlayer((java.util.UUID)Settings.Players.get(0));
       if (winner != null) {
        EndGame(winner);
       } else {
         EndGameNoWinner();
       }
     } else if (Settings.Players.size() <= 0) {
       EndGameNoWinner();
     }
   }
   
   public static void EndGame(final Player winner) {
     Settings.GameInProgress = false;
     Settings.PVPEnabled = false;
     
     Bukkit.broadcastMessage(ChatColor.AQUA + winner.getDisplayName() + " has won the game!");
     
     final Integer[] count = { Integer.valueOf(0) };
     
     new BukkitRunnable() {
       public void run() { Integer localInteger1;
        if (count[0].intValue() == 10) {
          GameHandler.StartServerRestartCountdown();
           cancel();
         } else {
           com.microcraftmc.uhc.utils.FireworkUtil.shootFirework(winner);
           localInteger1 = count[0];Integer[] arrayOfInteger = count;Integer localInteger2 = arrayOfInteger[0] = Integer.valueOf(arrayOfInteger[0].intValue() + 1); } } }.runTaskTimer(SugarUHC.getPlugin(), 0L, 10L);
   }
   
 
 
   public static void EndGameNoWinner()
   {
     Settings.GameInProgress = false;
     Settings.PVPEnabled = false;
     String Message = ChatColor.GREEN + "Game ended in a draw! ";
    if (Settings.Players.size() > 0) {
       Message = Message + "Congratulations ";
       for (java.util.UUID playerID : Settings.Players) {
         Player player = Bukkit.getPlayer(playerID);
         if (player != null) {
           Message = Message + player.getDisplayName() + ", ";
         }
       }
     }
     
     Bukkit.broadcastMessage(Message);
     StartServerRestartCountdown();
   }
 }


