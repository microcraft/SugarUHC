package com.microcraftmc.uhc.handlers;

import org.bukkit.entity.Player;

import java.util.ArrayList;

public class PlayerHandler {

    private static PlayerHandler instance = new PlayerHandler();

    private ArrayList<Player> playersList;
    private ArrayList<Player> UHCMods;
    private ArrayList<Player> UHCHosts;

    public PlayerHandler() {

    }

    public static PlayerHandler getInstance() {
        return instance;
    }


    public void setup() {
        playersList = new ArrayList<>();
        UHCMods = new ArrayList<>();
        UHCHosts = new ArrayList<>();
    }

    public boolean isUHCMod(Player player) {
        return UHCMods.contains(player);
    }

    public boolean isUHCHost(Player player) {
        return UHCHosts.contains(player);
    }

    public boolean isPlayer(Player player) {
        return playersList.contains(player);
    }

    public void addPlayer(Player player) {
        playersList.add(player);
    }

    public void removePlayer(Player player) {
        playersList.remove(player);
    }


    public void addUHCMod(Player player) {

        if (isPlayer(player)) {

            //remove them from the players list and add them to the uhc mods list
            removePlayer(player);
            UHCMods.add(player);

        }  else if (isUHCHost(player)) {

            //remove them from the uhc hosts list and add them to the uhc mods list
            removeUHCHost(player);
            UHCMods.add(player);

        }

    }

    public void removeUHCMod(Player player) {

        //is the player a uhc mod?
        if (isUHCMod(player)) {

            //remove them from the uhc mods list
            UHCMods.remove(player);

        }


    }

    public void addUHCHost(Player player) {

        if (isPlayer(player)) {

            //remove them from the players list and add them to the uhc host list
            removePlayer(player);
            UHCHosts.add(player);

        } else if (isUHCMod(player)) {

            //remove them from the uhc mod list and add them to the uhc host list
            removeUHCMod(player);
            UHCHosts.add(player);

        }

    }

    public void removeUHCHost(Player player) {

        //are they in the uhc host list?
        if (isUHCHost(player)) {

            //remove them from the uhc host list
            UHCHosts.remove(player);

        }

    }

    public Integer getPlayersLeft() {
        return playersList.size();
    }

    public ArrayList<Player> getPlayers() {
        return playersList;
    }

    public ArrayList<Player> getUHCMods() {
        return UHCMods;
    }

    public ArrayList<Player> getUHCHosts() {
        return UHCHosts;
    }
}
