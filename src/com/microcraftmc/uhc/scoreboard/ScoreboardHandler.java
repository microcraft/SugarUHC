 package com.microcraftmc.uhc.scoreboard;
 
 import com.microcraftmc.uhc.Settings;
 import org.bukkit.entity.Player;
 import org.bukkit.event.EventHandler;
 import org.bukkit.event.Listener;
 import org.bukkit.event.entity.EntityDamageEvent;
 import org.bukkit.scoreboard.DisplaySlot;
 import org.bukkit.scoreboard.Objective;
 import org.bukkit.scoreboard.Score;
 import org.bukkit.scoreboard.ScoreboardManager;
 import org.bukkit.scoreboard.Scoreboard;
 import org.bukkit.Bukkit;

 public class ScoreboardHandler implements Listener
 {
   @EventHandler
   public void onPlayerDamage(EntityDamageEvent event)
   {
        if (!(event.getEntity() instanceof Player)) return;
        if (!Settings.GameInProgress) return;
        if (!Settings.ShowHealthInTab) return;
        Player player = (Player)event.getEntity();
        Scoreboard board = Settings.Scoreboard;
        Objective obj = board.getObjective(DisplaySlot.PLAYER_LIST);
        Score score = obj.getScore(player);
        score.setScore((int)player.getHealth());
   }
   
   public static boolean setDeadScoreboard(Player p) {
        Scoreboard board = Settings.Scoreboard;
        Objective obj = board.getObjective(DisplaySlot.PLAYER_LIST);
        board.resetScores(p);
        return true;
   }
   
   public static boolean activateScoreboard(Player p)
   {
        Scoreboard board = Settings.Scoreboard;
        Objective obj = board.getObjective(DisplaySlot.PLAYER_LIST);
     
        Score score = obj.getScore(p);
        score.setScore((int)p.getHealth());
        p.setScoreboard(board);
        return true;
   }
   
   public static boolean createScoreboard() {
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        Scoreboard board = manager.getNewScoreboard();
        Objective obj = board.registerNewObjective("test", "dummy");
        obj.setDisplayName("Ultra Hardcore");
        obj.setDisplaySlot(DisplaySlot.PLAYER_LIST);
        Settings.Scoreboard = board;
        return true;
   }
 }

