package com.microcraftmc.uhc.scoreboard;

import com.microcraftmc.uhc.Settings;
import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.handlers.GameHandler;
import com.microcraftmc.uhc.handlers.PlayerHandler;
import com.microcraftmc.uhc.utils.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;


public class GameScoreboardManager {

    private static GameScoreboardManager instance = new GameScoreboardManager();
    private static Scoreboard scoreboardGame;
    private static Objective objective;
    private boolean countdownRunning;

    public static GameScoreboardManager getInstance() {
        return instance;
    }

    //setup Lobby scoreboardLobby
    public void setupScoreboardGame(Player player) {
        scoreboardGame = Bukkit.getScoreboardManager().getNewScoreboard();

        objective = scoreboardGame.registerNewObjective("game", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + " Microcraft SugarUHC ");

    }

    /*
     *
     * scoreboardLobby setup and updates
     *
     * This will update scoreboardLobby for all players
     */
    public void updateScoreboardGame() {
        for (Player players : Bukkit.getServer().getOnlinePlayers()) {
            if (players.getScoreboard().getObjective("game") == null) {
                setupScoreboardGame(players);
                players.setScoreboard(scoreboardGame);
            }
            if (players.getScoreboard().getObjective("game") != null) {
                players.setScoreboard(scoreboardGame); //Get scoreboardLobby
                objective.getScore(ChatColor.BOLD + "     ").setScore(15);
                objective.getScore(ChatColor.AQUA + "" + "Game Time").setScore(14);
                objective.getScore(GameTimerDisplay()).setScore(13);
                objective.getScore(ChatColor.BOLD + "    ").setScore(12);
                objective.getScore(ChatColor.AQUA + "" + "Players Left").setScore(11);
                objective.getScore(playersLeftDisplay()).setScore(10);
                objective.getScore(ChatColor.BOLD + "   ").setScore(9);
                objective.getScore(ChatColor.GOLD + "Current Border").setScore(8);
                objective.getScore(CurrentBorderDisplay()).setScore(7);
                objective.getScore(ChatColor.BOLD + "  ").setScore(6);
            }
        }
    }


    private static String playersLeftDisplay() {
        return ChatColor.WHITE + PlayerHandler.getInstance().getPlayersLeft().toString();
    }

    private static String GameTimerDisplay() {
       return ChatColor.WHITE + TimeUtil.timeConversion(Settings.GameTimer);
    }

    private static String CurrentBorderDisplay() {
        return ChatColor.WHITE + "N/A";
    }


}
