package com.microcraftmc.uhc.scoreboard;

import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.Settings;
import com.microcraftmc.uhc.handlers.GameHandler;
import com.microcraftmc.uhc.config.MessageManager;
import com.microcraftmc.uhc.config.MessageManager.PrefixType;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.ChatColor;


public class LobbyScoreboardManager {

    private static LobbyScoreboardManager instance = new LobbyScoreboardManager();
    private static Scoreboard scoreboardLobby;
    private static Objective objective;
    private boolean countdownRunning;

    public static LobbyScoreboardManager getInstance() {
        return instance;
    }

    //setup Lobby scoreboardLobby
    public void setupScoreboardLobby(Player player) {
        scoreboardLobby = Bukkit.getScoreboardManager().getNewScoreboard();

        objective = scoreboardLobby.registerNewObjective("lobby", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        if (!ConfigManager.getInstance().isDebugEnabled()) {
            if (Bukkit.getServer().getOnlinePlayers().size() < ConfigManager.getInstance().getConfig().getInt("Min Players")) {
                objective.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "UHC Devs");
            } else {
                this.countdown(60);
            }
        } else {
            objective.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "UHC Devs");
        }

    }

    /*
     *
     * scoreboardLobby setup and updates
     *
     * This will update scoreboardLobby for all players
     */
    public void updateScoreboardLobby() {
        for (Player players : Bukkit.getServer().getOnlinePlayers()) {
            if (players.getScoreboard().getObjective("lobby") == null) {
                setupScoreboardLobby(players);
                players.setScoreboard(scoreboardLobby);
            }
            if (players.getScoreboard().getObjective("lobby") != null) {
                players.setScoreboard(scoreboardLobby); //Get scoreboardLobby
                objective.getScore(ChatColor.BOLD + "     ").setScore(15);
                objective.getScore(ChatColor.AQUA + "" + "Server").setScore(14);
                objective.getScore(serverDisplay()).setScore(13);
                objective.getScore(ChatColor.BOLD + "    ").setScore(12);
                objective.getScore(ChatColor.YELLOW + "" + "Players").setScore(11);
                objective.getScore(playersDisplay()).setScore(10);
                objective.getScore(ChatColor.BOLD + "   ").setScore(9);
            }
        }
    }

    private static String serverDisplay() {
        return ChatColor.WHITE + ConfigManager.getInstance().getConfig().getString("Server Name");
    }

    private static String playersDisplay() {
       return Integer.toString(Bukkit.getServer().getOnlinePlayers().size()) + "/" +
               Integer.toString(ConfigManager.getInstance().getConfig().getInt("Max Players"));
    }

    public void updateScoreboardLobbyWithTime(String timerMsg) {

        for (Player players : Bukkit.getServer().getOnlinePlayers()) {
            if (players.getScoreboard().getObjective("lobby") == null) {
                setupScoreboardLobby(players);
                players.setScoreboard(scoreboardLobby);
            }
            if (players.getScoreboard().getObjective("lobby") != null) {
                players.setScoreboard(scoreboardLobby); //Get scoreboardLobby
                objective.setDisplayName(timerMsg);
            }
        }
    }

    public void updateScoreboardLobbyWithWaitingMsg() {

        for (Player players : Bukkit.getServer().getOnlinePlayers()) {
            if (players.getScoreboard().getObjective("lobby") == null) {
                setupScoreboardLobby(players);
                players.setScoreboard(scoreboardLobby);
            }
            if (players.getScoreboard().getObjective("lobby") != null) {
                players.setScoreboard(scoreboardLobby); //Get scoreboardLobby
            }
        }
    }

    public void hideScoreboardLobby() {
        for (Player players : Bukkit.getServer().getOnlinePlayers()) {
            scoreboardLobby = Bukkit.getScoreboardManager().getNewScoreboard();

            objective = scoreboardLobby.registerNewObjective("lobby", "dummy");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
            players.setScoreboard(scoreboardLobby);
        }
    }


    int count = 60;
    int tid = 0;
    public void countdown(int time) {
        //Bukkit.broadcastMessage(""+time);
        countdownRunning = true;
        count = time;
        Bukkit.getScheduler().cancelTask(tid);

        tid = Bukkit.getScheduler().scheduleSyncRepeatingTask(SugarUHC.getPlugin(), new Runnable() {
            public void run() {

                    while (Bukkit.getServer().getOnlinePlayers().size() >= ConfigManager.getInstance().getConfig().getInt("Min Players")) {
                        if (count > 0) {
                            if (count < 10) {
                                for (Player players : Bukkit.getServer().getOnlinePlayers()) {
                                    players.playSound(players.getLocation(), Sound.ORB_PICKUP, 1, 0);
                                }
                            }
                            updateScoreboardLobbyWithTime(ChatColor.WHITE + "Starting In " + ChatColor.GREEN + count + " seconds");
                            count--;
                        } else {
                            GameHandler.getInstance().StartGameCountdown();
                            Bukkit.getScheduler().cancelTask(tid);
                            countdownRunning = false;
                        }
                    }

                    updateScoreboardLobbyWithWaitingMsg();
                    Bukkit.getScheduler().cancelTask(tid);

            }}, 0, 20);


    }

}