 package com.microcraftmc.uhc.commands;

 import com.microcraftmc.uhc.Settings;
 import com.microcraftmc.uhc.SugarUHC;

 import com.microcraftmc.uhc.config.ConfigManager;
 import com.microcraftmc.uhc.config.MessageManager;
 import org.bukkit.entity.Player;

 public class DebugCommand implements SubCommand
 {
    public boolean onCommand(Player player, String[] args)
    {
        if (player.isOp()) {
            if (args.length != 1) {
                MessageManager.getInstance().sendMessage(MessageManager.PrefixType.WARNING, "Valid debug types <enable | disable | status>", player);
                return true;
            }

            if (args[0].equalsIgnoreCase("enable")) {
                try {

                    //enable Debug Mode
                    ConfigManager.getInstance().getConfig().set("debug", true);
                    ConfigManager.getInstance().saveConfig();
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, "Debug mode has been enabled.", player);
                    return true;

                    } catch (Exception e) {
                        e.printStackTrace();
                        MessageManager.getInstance().sendMessage(MessageManager.PrefixType.ERROR, "An unexpected error has occurred, check your logs for details.", player);
                        return false;
                }

            } else if (args[0].equalsIgnoreCase("disable")) {
                try {

                    //disable Debug Mode
                    ConfigManager.getInstance().getConfig().set("debug", false);
                    ConfigManager.getInstance().saveConfig();
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, "Debug mode has been disabled.", player);
                    return true;

                } catch (Exception e) {
                    e.printStackTrace();
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.ERROR, "An unexpected error has occured, check your logs for details.", player);
                    return false;
                }
            } else if(args[0].equalsIgnoreCase("status")) {
                if(!ConfigManager.getInstance().isDebugEnabled()) {
                    //debug mode is disabled
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, "Debug mode is not enabled.", player);
                } else {
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, "Debug mode is enabled.", player);
                }
            }

        } else {
            MessageManager.getInstance().sendFMessage(MessageManager.PrefixType.WARNING, "error.nopermission", player);
        }

        return true;
    }

     @Override
     public String help(Player p) {
         return "use /debug <enable | disable | status> to enable/disable debug mode or view the status of debug mode";
     }

     @Override
     public String permission() {
         return "uhc.admin.debug";
     }
 }


