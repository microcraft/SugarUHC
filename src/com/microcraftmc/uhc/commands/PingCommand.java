 package com.microcraftmc.uhc.commands;

 import com.microcraftmc.uhc.config.MessageManager;
 import com.microcraftmc.uhc.config.MessageManager.PrefixType;

 import com.microcraftmc.uhc.utils.PingUtil;
 import org.bukkit.Bukkit;
 import org.bukkit.command.Command;
 import org.bukkit.command.CommandExecutor;
 import org.bukkit.command.CommandSender;
 import org.bukkit.entity.Player;

 public class PingCommand implements CommandExecutor
 {
   public boolean onCommand(CommandSender sender, Command command, String string, String[] args) {

       if (sender instanceof Player) {

           Player player = (Player) sender;

           if (command.getName().equalsIgnoreCase("ping")) {

               if (args == null || args.length < 1) {
                   //MessageManager.getInstance().sendMessage(PrefixType.INFO, "SugarUHC Version " + SugarUHC.getPlugin().getDescription().getVersion(), player);
                   //MessageManager.getInstance().sendMessage(PrefixType.INFO, "Type /uhc help <player | staff | admin> for command information", player);
                   //return true;
                   //open the whitelist gui by default unless a command is specified
                   try {
                       MessageManager.getInstance().sendMessage(PrefixType.INFO, String.format(
                            "Your ping is %s", PingUtil.getInstance().getPlayerPing(player)), player);
                   } catch (Exception e) {
                       e.printStackTrace();
                   }
                   return true;
               }


               if (args[0].equalsIgnoreCase("player")) {

                       if (args[1] != null) {

                           if (player.isOp()) {

                               Player playerRequested = Bukkit.getPlayer(args[1]);

                               if (playerRequested != null) {

                                   try {
                                       MessageManager.getInstance().sendMessage(PrefixType.INFO, String.format(
                                               "The players ping is %s", PingUtil.getInstance().getPlayerPing(playerRequested)), player);
                                   } catch (Exception e) {
                                       e.printStackTrace();
                                   }
                               } else  {
                                   MessageManager.getInstance().sendMessage(PrefixType.ERROR, "The player " + args[1] + " could not be found", player);
                               }


                           } else {
                               MessageManager.getInstance().sendMessage(PrefixType.ERROR, "You do not have permissions to check player pings", player);
                           }

                       } else {
                           MessageManager.getInstance().sendMessage(PrefixType.ERROR, "To check a players ping, use /ping player [Player]", player);
                       }

               }

           }

       }

       return true;
   }

 }


