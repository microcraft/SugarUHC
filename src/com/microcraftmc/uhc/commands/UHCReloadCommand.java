 package com.microcraftmc.uhc.commands;

 import com.microcraftmc.uhc.Settings;
 import com.microcraftmc.uhc.SugarUHC;
 import org.bukkit.ChatColor;
 import org.bukkit.command.Command;
 import org.bukkit.command.CommandExecutor;
 import org.bukkit.command.CommandSender;

 public class UHCReloadCommand implements CommandExecutor
 {
   public boolean onCommand(CommandSender sender, Command command, String string, String[] args)
   {
     if (!sender.isOp()) {
       sender.sendMessage(ChatColor.RED + "You do not have permission to run that command");
      return false;
     }

     if (command.getName().equalsIgnoreCase("uhcreload")) {
         SugarUHC.getPlugin().reloadUHCConfig();
         sender.sendMessage(ChatColor.GREEN + "Reloaded Config Successfully!");
     }
     return false;
   }
 }


