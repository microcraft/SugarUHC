 package com.microcraftmc.uhc.commands;
 
 import com.microcraftmc.uhc.Settings;
 import org.bukkit.ChatColor;
 import org.bukkit.command.Command;
 import org.bukkit.command.CommandExecutor;
 import org.bukkit.command.CommandSender;
 
 public class TimeLeftCommand implements CommandExecutor
 {
   public boolean onCommand(CommandSender sender, Command command, String string, String[] args)
   {
     if (!Settings.GameInProgress) {
       sender.sendMessage(ChatColor.RED + "The game is currently not in progress");
       return false;
     }
     
     if (Settings.TimeLeft.intValue() > 60) {
      Integer minutes = Integer.valueOf(Settings.TimeLeft.intValue() / 60);
      sender.sendMessage(ChatColor.GREEN + "There is " + minutes + " minutes until the game ends");
       return true;
     }
     sender.sendMessage(ChatColor.GREEN + "There is " + Settings.TimeLeft + " seconds until the game ends");
    return true;
   }
 }


