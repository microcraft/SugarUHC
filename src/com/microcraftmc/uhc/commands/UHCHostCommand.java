 package com.microcraftmc.uhc.commands;

 import com.microcraftmc.uhc.config.MessageManager;
 import com.microcraftmc.uhc.handlers.PlayerHandler;
 import org.bukkit.GameMode;
 import org.bukkit.entity.Player;


 public class UHCHostCommand implements SubCommand
 {
     public boolean onCommand(Player player, String[] args)
     {
         if (player.isOp()) {
             try {
                if (!PlayerHandler.getInstance().isUHCHost(player)) {
                    player.setGameMode(GameMode.SPECTATOR);
                    PlayerHandler.getInstance().addUHCHost(player);
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, "uhc host mode enabled, you are now hosting a uhc match.", player);
                    return false;
                } else {
                    player.setGameMode(GameMode.SURVIVAL);
                    PlayerHandler.getInstance().removeUHCHost(player);
                    PlayerHandler.getInstance().addPlayer(player);
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, "uhc host mode disabled.", player);
                    return false;
                }

             } catch (Exception e) {
                 e.printStackTrace();
                 MessageManager.getInstance().sendMessage(MessageManager.PrefixType.ERROR, "An unexpected error has occurred, check your logs for details.", player);
             }
         }

         return true;
     }

     @Override
     public String help(Player p) {
         return "use /uhc host to host the uhc game";
     }

     @Override
     public String permission() {
         return "uhc.staff.host";
     }
 }


