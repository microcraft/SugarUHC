 package com.microcraftmc.uhc.commands;
 
 import com.microcraftmc.uhc.handlers.GameHandler;
 import com.microcraftmc.uhc.Settings;
 import org.bukkit.ChatColor;
 import org.bukkit.command.Command;
 import org.bukkit.command.CommandExecutor;
 import org.bukkit.command.CommandSender;
 
 public class MeetUpCommand implements CommandExecutor
 {
   public boolean onCommand(CommandSender sender, Command command, String string, String[] args)
   {
    if (!sender.isOp()) {
       sender.sendMessage(ChatColor.RED + "You do not have permission to run that command");
       return false;
     }
     
     if (!Settings.GameInProgress) {
       sender.sendMessage(ChatColor.RED + "The game is not in progress");
       return false;
     }
     
    if (Settings.GameInMeetup) {
       sender.sendMessage(ChatColor.RED + "The game is already in meet up");
       return false;
     }
     
     sender.sendMessage(ChatColor.GREEN + "Starting meetup...");
     GameHandler.StartMeetUp();
     return false;
   }
 }


