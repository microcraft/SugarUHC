 package com.microcraftmc.uhc.commands;

 import com.microcraftmc.uhc.config.ConfigManager;
 import com.microcraftmc.uhc.config.LobbyManager;
 import com.microcraftmc.uhc.config.MessageManager;
 import org.bukkit.entity.Player;
 import org.bukkit.ChatColor;
 import org.bukkit.Location;


 public class SetLobbyCommand implements SubCommand
 {

     public boolean onCommand(Player player, String[] args) {
         if (player.isOp()) {
             try {
                 LobbyManager.getInstance().setLobbySpawn(player.getLocation());
                 MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, "The lobby spawn has been set successfully.", player);
                 return true;

             } catch (Exception e) {
                 e.printStackTrace();
                 MessageManager.getInstance().sendMessage(MessageManager.PrefixType.ERROR, "An unexpected error has occurred, check your logs for details.", player);
             }
         }
         return true;
     }


     @Override
     public String help(Player p) {
         return "use /uhc setlobby to set the lobby for the uhc spawn";
     }

     @Override
     public String permission() {
         return "uhc.admin.setlobby";
     }

 }


