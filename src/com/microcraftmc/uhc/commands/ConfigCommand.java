 package com.microcraftmc.uhc.commands;

 import com.microcraftmc.uhc.Settings;
 import com.microcraftmc.uhc.SugarUHC;
 import com.microcraftmc.uhc.config.MessageManager;
 import com.microcraftmc.uhc.config.MessageManager.PrefixType;
 import com.microcraftmc.uhc.features.FeatureManager;
 import com.microcraftmc.uhc.features.UHCFeature;
 import com.microcraftmc.uhc.features.UHCFeatureList;
 import com.microcraftmc.uhc.handlers.PlayerHandler;
 import org.bukkit.ChatColor;
 import org.bukkit.entity.Player;

 import java.util.Iterator;

 public class ConfigCommand implements SubCommand
 {
    public boolean onCommand(Player player, String[] args)
    {

        if (player.isOp() && PlayerHandler.getInstance().isUHCHost(player)) {

            if (args[0] != null) {

                if (args[0].equalsIgnoreCase("nether")) {

                    if (args[1] != null) {

                        if (args[1].equalsIgnoreCase("on")) {

                            try {
                                Object netherFeature = FeatureManager.getInstance().getFeature("DisableNether");
                                ((UHCFeature)netherFeature).setEnabled(true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (args[1].equalsIgnoreCase("off")) {

                            try {
                                Object netherFeature = FeatureManager.getInstance().getFeature("DisableNether");
                                ((UHCFeature)netherFeature).setEnabled(false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    }

                }

                if (args[0].equalsIgnoreCase("features")) {

                    Object featuresList = FeatureManager.getInstance().getFeatures();
                    MessageManager.getInstance().sendMessage(PrefixType.INFO, "Currently loaded features: " + ((UHCFeatureList)featuresList).size(), player );
                    if (((UHCFeatureList)featuresList).size() == 0) {
                        MessageManager.getInstance().sendMessage(PrefixType.ERROR, "No features that are loaded have been detected.", player);
                    }
                    Iterator featuresIterator = ((UHCFeatureList)featuresList).iterator();
                    while (featuresIterator.hasNext()) {
                        UHCFeature uhcFeature = (UHCFeature)featuresIterator.next();
                        MessageManager.getInstance().sendMessage(PrefixType.INFO,
                                (uhcFeature.isEnabled() ? ChatColor.GREEN + "ON " : new StringBuilder().append(ChatColor.RED).append("OFF ").toString()) + uhcFeature.getFeatureID() + ChatColor.GRAY + " - " + uhcFeature.getDescription(), player);
                    }
                    return true;

                }

            }

        } else {
            MessageManager.getInstance().sendMessage(PrefixType.ERROR, "You must be in host mode to use /uhc config commands", player);
        }

        return true;
    }

     @Override
     public String help(Player p) {
        return "use /uhc config to manage game settings";
    }

     @Override
     public String permission() {
         return "uhc.host.start";
     }
 }


