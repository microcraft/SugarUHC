 package com.microcraftmc.uhc.commands;

 import com.microcraftmc.uhc.config.MessageManager;
 import com.microcraftmc.uhc.handlers.PlayerHandler;
 import org.bukkit.GameMode;
 import org.bukkit.entity.Player;


 public class UHCModCommand implements SubCommand
 {
     public boolean onCommand(Player player, String[] args)
     {
         if (player.isOp()) {
             try {

                 player.setGameMode(GameMode.SPECTATOR);
                 PlayerHandler.getInstance().addUHCMod(player);
                 MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, "uhc mod mode enabled, you are now modding a uhc match.", player);
                 return false;

             } catch (Exception e) {
                 e.printStackTrace();
                 MessageManager.getInstance().sendMessage(MessageManager.PrefixType.ERROR, "An unexpected error has occurred, check your logs for details.", player);
             }
         }

         return true;
     }

     @Override
     public String help(Player p) {
         return "use /uhc mod to mod the uhc game";
     }

     @Override
     public String permission() {
         return "uhc.staff.mod";
     }
 }


