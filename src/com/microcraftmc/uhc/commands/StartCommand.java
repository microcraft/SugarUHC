 package com.microcraftmc.uhc.commands;
 
 import com.microcraftmc.uhc.SugarUHC;
 import com.microcraftmc.uhc.config.ConfigManager;
 import com.microcraftmc.uhc.config.MessageManager;
 import com.microcraftmc.uhc.handlers.GameHandler;
 import com.microcraftmc.uhc.Settings;
 import com.microcraftmc.uhc.scoreboard.LobbyScoreboardManager;
 import com.microcraftmc.uhc.tasks.RulesListTask;
 import com.microcraftmc.uhc.tasks.ScoreboardCountdownTask;
 import org.bukkit.Bukkit;
 import org.bukkit.ChatColor;
 import org.bukkit.entity.Player;

 public class StartCommand implements SubCommand
 {
    public boolean onCommand(Player player, String[] args)
    {

        if (player.isOp()) {
            try {

                if (Settings.GameInProgress) {
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.ERROR, "The game is already in progress.", player);
                    return false;
                }

                MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, "Starting game...", player);
                try {
                    //GameHandler.getInstance().GameCountdown(170);
                    //Bukkit.getScheduler().scheduleSyncRepeatingTask(SugarUHC.getPlugin(), new ScoreboardCountdownTask(), 0, 20);
                    ScoreboardCountdownTask.getInstance().runTaskTimer(SugarUHC.getPlugin(), 0, 20);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;

            } catch (Exception e) {
                e.printStackTrace();
                MessageManager.getInstance().sendMessage(MessageManager.PrefixType.ERROR, "An unexpected error has occurred, check your logs for details.", player);
            }
        }

        return true;
    }

     @Override
     public String help(Player p) {
        return "use /uhc start to force start the uhc game";
    }

     @Override
     public String permission() {
         return "uhc.host.start";
     }
 }


