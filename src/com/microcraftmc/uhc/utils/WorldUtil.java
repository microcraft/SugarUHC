 package com.microcraftmc.uhc.utils;
 
 import com.microcraftmc.uhc.SugarUHC;
 import com.microcraftmc.uhc.Settings;
 import com.sk89q.worldedit.CuboidClipboard;
 import com.sk89q.worldedit.EditSession;
 import com.sk89q.worldedit.MaxChangedBlocksException;
 import com.sk89q.worldedit.Vector;
 import com.sk89q.worldedit.bukkit.BukkitWorld;
 import java.io.ByteArrayOutputStream;
 import java.io.DataOutputStream;
 import java.io.File;
 import java.io.IOException;


 import org.bukkit.Bukkit;
 import org.bukkit.Difficulty;
 import org.bukkit.World;
 import org.bukkit.WorldCreator;
 import org.bukkit.block.Biome;
 import org.bukkit.entity.Entity;
 import org.bukkit.entity.Player;
 import org.bukkit.plugin.java.JavaPlugin;
 
 public class WorldUtil extends JavaPlugin
 {
   private static SugarUHC p = SugarUHC.getPlugin();

   public static void loadSpawn(String name)
   {
        World getWorld = p.getServer().getWorld(name);
        File file = new File("plugins/SugarUHC/" + Settings.LobbyName + ".schematic");
        Vector v = new Vector(0, 200, 0);
        try
        {
            loadArea(getWorld, file, v);
        } catch (com.sk89q.worldedit.data.DataException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MaxChangedBlocksException e) {
            e.printStackTrace();
        } catch (com.sk89q.worldedit.world.DataException e) {
            e.printStackTrace();
        }
   }
   
   public static void clearSpawn(String name) {
        World getWorld = p.getServer().getWorld(name);
        File file = new File("plugins/UltraHardcore/clear.schematic");
        Vector v = new Vector(0, 200, 0);
        try
        {
            loadArea(getWorld, file, v);
        } catch (com.sk89q.worldedit.data.DataException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MaxChangedBlocksException e) {
            e.printStackTrace();
        } catch (com.sk89q.worldedit.world.DataException e) {
            e.printStackTrace();
        }
   }
   
   public static void loadArea(World world, File file, Vector origin) throws com.sk89q.worldedit.world.DataException, IOException, MaxChangedBlocksException {
        EditSession es = new EditSession(new BukkitWorld(world), 999999999);
        CuboidClipboard cc = CuboidClipboard.loadSchematic(file);
        cc.paste(es, origin, false);
   }
   
   public static void loadAreaNA(World world, File file, Vector origin) throws com.sk89q.worldedit.world.DataException, IOException, MaxChangedBlocksException {
        Biome biome = world.getBiome((int)origin.getX(), (int)origin.getZ());
     
        EditSession es = new EditSession(new BukkitWorld(world), 999999999);
        CuboidClipboard cc = CuboidClipboard.loadSchematic(file);
     
 
        if (biome.name().toLowerCase().contains("ocean")) {
            cc.paste(es, origin, false);
        } else {
            cc.paste(es, origin, true);
        }
   }
   
   public static boolean deleteWorld(String name) {

        World world = p.getServer().getWorld(name);
        if (world != null) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.sendMessage("Thanks for playing Ultra Hardcore");
                ByteArrayOutputStream b = new ByteArrayOutputStream();
                DataOutputStream out = new DataOutputStream(b);
                try {
                    out.writeUTF("Connect");
                    out.writeUTF("UHCLobby");
                } catch (IOException e) {
                    e.printStackTrace();
                }
         
                player.sendPluginMessage(p, "BungeeCord", b.toByteArray());
            }
       
            p.getServer().getLogger().info("Old UHC world found...");
            p.getServer().unloadWorld(name, true);
            File directory = new File(name);

            if (directory.exists()) {
                try {
                    p.getServer().getLogger().info("Deleting old UHC world...");
                    delete(directory);
                    p.getServer().getLogger().info("Deleted old UHC world");
                    return true;
                } catch (IOException ignored) {
                    return false;
                }
            }
            return true;
        }
     
        return true;
   }
   
   public static World createWorld(String name)
   {
        return createWorld(name, false);
   }
   
   public static World createWorld(String name, boolean overwrite) {

        if (Bukkit.getWorld(name) != null) {
            if (overwrite) {
                deleteWorld(name);
            } else {
                return null;
            }
        }

        WorldCreator worldCreator = new WorldCreator(name);
        World world = Bukkit.createWorld(worldCreator);
        world.setDifficulty(Difficulty.HARD);
        for (Entity entity : world.getEntities()) {
            if (entity.getType() != org.bukkit.entity.EntityType.PLAYER) {
                entity.remove();
            }
        }
        return world;
   }
   
   public static void delete(File file) throws IOException
   {
        if (file.isDirectory()) {
 
            if (file.list().length == 0) {
                file.delete();
                System.out.println("Directory is deleted : " + file.getAbsolutePath());
            } else {
                String[] files = file.list();
                for (String temp : files) {
                    File fileDelete = new File(file, temp);
                    delete(fileDelete);
                }
                if (file.list().length == 0) {
                    file.delete();
                    System.out.println("Directory is deleted : " + file.getAbsolutePath());
                }
            }
     } else {
       file.delete();
       System.out.println("File is deleted : " + file.getAbsolutePath());
     }

   }

 }


