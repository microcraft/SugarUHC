package com.microcraftmc.uhc.utils;


import com.microcraftmc.uhc.SugarUHC;
import org.bukkit.Bukkit;

import java.io.*;
import java.net.HttpURLConnection;

public class UpdateUtil {

    private static UpdateUtil instance = new UpdateUtil();

    public UpdateUtil() {

    }

    public static UpdateUtil getInstance() {
        return instance;
    }

    public boolean shouldUpdate(String version) throws IOException {

        String checkLocation = "https://files.microcraftmc.net/sugaruhc/version.txt";



        java.net.URL url = null;
        try {
            url = new java.net.URL(checkLocation);
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
        }

        HttpURLConnection con = null;
        try {
            assert (url != null);
            con = (HttpURLConnection)url.openConnection();
            con.addRequestProperty("User-Agent", "Mozilla/4.76");
        } catch (IOException e) {
            e.printStackTrace();
        }


        int responseCode = con.getResponseCode();
        System.out.println("Auto Update Response Code: " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));


        StringBuffer response = new StringBuffer();
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println("Auto Update Response: " + response);

        String result = String.valueOf(response);

        if ((!result.equalsIgnoreCase(version)) && (responseCode == 200))
        {
            return true;
        }

        return false;
   }
    
   public void installUpdate() throws IOException {
        String saveDir = "plugins";
        String link = "https://files.microcraftmc.net/sugaruhc/sugaruhc.jar";

        downloadFile(link, saveDir);
   }
    

   public void downloadFile(String fileURL, String saveDir) throws IOException {

        java.net.URL url = new java.net.URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection)url.openConnection();
        httpConn.addRequestProperty("User-Agent", "Mozilla/4.0");
        int responseCode = httpConn.getResponseCode();


        if (responseCode == 200) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {

                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10, disposition.length() - 1);
                }

            } else {

                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1, fileURL.length());

            }


            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);


            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + fileName;


            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte['?'];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();

            LogUtil.LogToConsole("Update downloaded");
            LogUtil.LogToConsole("Reloading Server...");

            Bukkit.getServer().reload();

        } else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode + ", URL: " + fileURL);
        }

        httpConn.disconnect();
   }

}
