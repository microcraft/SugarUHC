package com.microcraftmc.uhc.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.microcraftmc.uhc.config.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PingUtil
{
  private static PingUtil instance = new PingUtil();
  private static Class<?> craftPlayer;
  private static Method handle;
  private static Field pingField;

  public PingUtil() {

  }

  public static PingUtil getInstance() {
    return instance;
  }
  
  public void setup()
  {
      try
      {
          String serverVersion = getServerVersion();
          craftPlayer = Class.forName("org.bukkit.craftbukkit." + serverVersion +
            ".entity.CraftPlayer");
          Class<?> entityPlayer = Class.forName("net.minecraft.server." + serverVersion +
            ".EntityPlayer");
      
          handle = craftPlayer.getMethod("getHandle", new Class[0]);
      
          pingField = entityPlayer.getField("ping");
      }
      catch (Exception e)
      {
        LogUtil.LogToConsole("Error occurred while loading the plugin, are you using MCPC or Cauldron?");
        e.printStackTrace();
      }
  }
  
  public void msg(Player player, String message)
  {
    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.INFO, message, player);
  }
  
  public void log(String message)
  {
    LogUtil.LogToConsole(message);
  }
  
  public void warning(String message)
  {
    LogUtil.LogToConsole(message);
  }
  
  public int getPlayerPing(Player player) throws Exception
  {
    int ping = 0;
    
    Object converted = craftPlayer.cast(player);
    
    Object entityPlayer = handle.invoke(converted, new Object[0]);
    
    ping = pingField.getInt(entityPlayer);
    
    return ping;
  }
  
  public String getServerVersion()
  {
    Pattern brand = Pattern.compile("(v|)[0-9][_.][0-9][_.][R0-9]*");

    String pkg = Bukkit.getServer().getClass().getPackage().getName();
    String version = pkg.substring(pkg.lastIndexOf('.') + 1);
    if (!brand.matcher(version).matches()) {
      version = "";
    }
    return version;
  }
}
