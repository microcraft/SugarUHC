package com.microcraftmc.uhc.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import static org.bukkit.ChatColor.*;


public class ChatUtils {

    public static void broadcast(String msg) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            //send a message to each player
            player.sendMessage(starter() + msg);
        }
    }

    private static String starter() {
        return GOLD + "[" + DARK_GREEN + "SugarUHC" + GOLD + "] " + WHITE;
    }

}
