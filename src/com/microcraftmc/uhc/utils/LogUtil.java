package com.microcraftmc.uhc.utils;


import com.microcraftmc.uhc.SugarUHC;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.ConsoleCommandSender;


public class LogUtil
{
    private static HashMap<Integer, String> Logs = new HashMap();
    private static Integer i = Integer.valueOf(0);
    private static SugarUHC p = SugarUHC.getPlugin();
    Gson jsonHandler = new Gson();


    public static void LogToConsole(String message) {
        ConsoleCommandSender console = org.bukkit.Bukkit.getConsoleSender();
        console.sendMessage(message);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String date = df.format(today);
        Integer localInteger1;
        if ((message != null) && (!message.equalsIgnoreCase(""))) {
            Logs.put(i, "[" + date + "]: " + message);
            localInteger1 = i; Integer localInteger2 = i = Integer.valueOf(i.intValue() + 1);
        }
   }

   public static void ClearLogs() {
        i = Integer.valueOf(0);
        Logs.clear();
   }

}


