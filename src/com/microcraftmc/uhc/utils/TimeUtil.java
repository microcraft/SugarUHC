 package com.microcraftmc.uhc.utils;
 
 public class TimeUtil
 {
     public static String timeConversion(int totalSeconds) {

        int MINUTES_IN_AN_HOUR = 60;
        int SECONDS_IN_A_MINUTE = 60;
     
        int seconds = totalSeconds % 60;
        int totalMinutes = totalSeconds / 60;
        int minutes = totalMinutes % 60;
        int hours = totalMinutes / 60;
     
        String hoursStr = String.valueOf(hours);
        String minutesStr = String.valueOf(minutes);
        String secondsStr = String.valueOf(seconds);

        if (hours < 10) {
            hoursStr = "0" + hoursStr;
        }
     
        if (minutes < 10) {
            minutesStr = "0" + minutesStr;
        }
     
        if (seconds < 10) {
            secondsStr = "0" + secondsStr;
        }
     
        if ((minutes == 0) && (hours == 0)) {
            return seconds + " seconds";
        }

         if (hours == 0) {
            return minutesStr + ":" + secondsStr;
         }
     
        return hoursStr + ":" + minutesStr + ":" + secondsStr;
   }
 }


