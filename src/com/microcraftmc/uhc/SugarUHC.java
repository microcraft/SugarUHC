package com.microcraftmc.uhc;

import com.microcraftmc.uhc.commands.*;
import com.microcraftmc.uhc.config.CommandManager;
import com.microcraftmc.uhc.config.LobbyManager;
import com.microcraftmc.uhc.config.MessageManager;
import com.microcraftmc.uhc.features.FeatureManager;
import com.microcraftmc.uhc.gui.WhitelistGUI;
import com.microcraftmc.uhc.gui.WhitelistRemovePlayersGUI;
import com.microcraftmc.uhc.handlers.PlayerHandler;
import com.microcraftmc.uhc.scoreboard.ScoreboardHandler;
import com.microcraftmc.uhc.utils.LogUtil;
import com.microcraftmc.uhc.listeners.*;
import com.microcraftmc.uhc.utils.PingUtil;
import com.microcraftmc.uhc.utils.UpdateUtil;
import com.microcraftmc.uhc.utils.WorldUtil;
import com.microcraftmc.uhc.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.Listener;

import javax.persistence.Lob;
import java.io.IOException;

public class SugarUHC extends JavaPlugin implements Listener
 {
   private static SugarUHC p;
   public static boolean Snapshot = false;

   public void onEnable() {
        p = this;
        if (getDescription().getVersion().toLowerCase().contains("snapshot")) {
           Snapshot = true;
        }

        //attempt to load our config
        this.loadConfig();

        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        registerRecipes();

        ConfigManager.getInstance().setup();
        MessageManager.getInstance().setup();
        LobbyManager.getInstance().setup();
        PlayerHandler.getInstance().setup();
        FeatureManager.getInstance().setup();
        PingUtil.getInstance().setup();

        registerListener(Bukkit.getPluginManager());
        registerCommands();

        logStartup();

        WorldUtil.createWorld("UHC_World", true);

       String version = getDescription().getVersion();


        //check for updates
        try {

            if (UpdateUtil.getInstance().shouldUpdate(getDescription().getVersion())) {
                UpdateUtil.getInstance().installUpdate();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!ConfigManager.getInstance().isDebugEnabled()) {

            if (Settings.ShowHealthInTab) {
                ScoreboardHandler.createScoreboard();
            }

            //com.microcraftmc.uhc.handlers.GameHandler.StartGameCountdown();
        }
   }

   public void onDisable() {
     for (Player p : Bukkit.getOnlinePlayers()) {
       p.kickPlayer(ChatColor.RED + "" + ChatColor.BOLD + "Ultra Hardcore is rebooting.");
     }

     PlayerHandler.getInstance().getPlayers().clear();
     PlayerHandler.getInstance().getUHCMods().clear();
     PlayerHandler.getInstance().getUHCHosts().clear();


     if (Snapshot) {
      LogUtil.LogToConsole("Sending logs to Hastebin as you are running a snapshot build...");
     LogUtil.ClearLogs();
    }


    WorldUtil.deleteWorld("UHC_World");

     getLogger().info(getDescription().getName() + " has been disabled!");
  }

   public static SugarUHC getPlugin() {
    return p;
   }

   private void logStartup() {

       LogUtil.LogToConsole("");
       LogUtil.LogToConsole(ChatColor.BLUE + "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
       LogUtil.LogToConsole("");
       LogUtil.LogToConsole(ChatColor.AQUA + "SugarUHC");
       LogUtil.LogToConsole(ChatColor.AQUA + "Version " + getDescription().getVersion());
       LogUtil.LogToConsole("");
       LogUtil.LogToConsole(ChatColor.BLUE + "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
       LogUtil.LogToConsole("");

   }

   private void loadConfig()
   {
       getConfig().options().copyDefaults(true);
       saveDefaultConfig();
       Settings.SpeedMode = getConfig().getBoolean("Speed Mode");
       Settings.ShowHeartsInTab = getConfig().getBoolean("Show Hearts In Tab Menu");
       Settings.ShowHealthInTab = getConfig().getBoolean("Show Health In Tab Menu");
       Settings.GameTime = getConfig().getInt("Game Length");
       Settings.PVPTime = getConfig().getInt("Game PVP Wait Length");
       Settings.MinPlayers = getConfig().getInt("Min Players");
       Settings.SpectatingEnabled = getConfig().getBoolean("Spectating Enabled");
       Settings.ShowTimeInTab = getConfig().getBoolean("Show Time Left In Tab Menu");
       Settings.MapRadius = getConfig().getInt("Map Radius");
       Settings.LobbyName = getConfig().getString("Lobby Name");
       Settings.DebugMode = getConfig().getBoolean("Development Mode");
       Settings.LobbyWorld = getConfig().getString("Lobby World");
       Settings.LobbySpawnX = getConfig().getDouble("Lobby Spawn.x");
       Settings.LobbySpawnZ = getConfig().getDouble("Lobby Spawn.z");
       Settings.LobbySpawnY = getConfig().getDouble("Lobby Spawn.y");
       Settings.Title = getConfig().getString("Title");
       Settings.Subtitle = getConfig().getString("Subtitle");
       Settings.TabTitle = getConfig().getString("Tab Header");
       Settings.TabFooter = getConfig().getString("Tab Footer");
       Settings.titleFadeInTime = getConfig().getInt("Title Fade.In");
       Settings.titleFadeOutTime = getConfig().getInt("Title Fade.Out");
       Settings.titleStayTime = getConfig().getInt("Title Fade.Stay");
   }

   public void reloadUHCConfig()
   {
       reloadConfig();
       loadConfig();
   }


   private void registerRecipes() {
        ShapedRecipe goldenapple = new ShapedRecipe(new ItemStack(Material.GOLDEN_APPLE, 1));

        goldenapple.shape(new String[] { "GGG", "GAG", "GGG" });
        goldenapple.setIngredient('G', Material.GOLD_INGOT);
        goldenapple.setIngredient('A', Material.APPLE);

        getServer().addRecipe(goldenapple);

        ShapedRecipe goldenapple2 = new ShapedRecipe(new ItemStack(Material.GOLDEN_APPLE, 1));

        goldenapple2.shape(new String[]{"GGG", "GAG", "GGG"});
        goldenapple2.setIngredient('G', Material.GOLD_INGOT);
        goldenapple2.setIngredient('A', Material.SKULL_ITEM, 3);

        getServer().addRecipe(goldenapple2);

        ShapedRecipe oldgoldenapple = new ShapedRecipe(new ItemStack(Material.APPLE, 1, (short)100));

        oldgoldenapple.shape(new String[]{"GGG", "GAG", "GGG"});
        oldgoldenapple.setIngredient('G', Material.GOLD_NUGGET);
        oldgoldenapple.setIngredient('A', Material.APPLE);

        getServer().addRecipe(oldgoldenapple);
  }

  private void registerListener(PluginManager pluginManager) {

      final ServerPing serverPingListener = new ServerPing();
      final PlayerLogin playerLoginListener = new PlayerLogin();
      final PlayerJoin playerJoinListener = new PlayerJoin();
      final PlayerDeath playerDeathListener = new PlayerDeath();
      final PlayerDamage playerDamageListener = new PlayerDamage();
      final PlayerHunger playerHungerListener = new PlayerHunger();
      final PlayerPVP playerPVPListener = new PlayerPVP();
      final PlayerHealth playerHealthListener = new PlayerHealth();
      final PlayerQuit playerQuitListener = new PlayerQuit();
      final BlockBreak blockBreakListener = new BlockBreak();
      final PlayerChat playerChat = new PlayerChat();
      final WhitelistGUI whitelistGUI = new WhitelistGUI();
      final WhitelistRemovePlayersGUI whitelistRemovePlayersGUI = new WhitelistRemovePlayersGUI();

      //register the events
      pluginManager.registerEvents(serverPingListener, this);
      pluginManager.registerEvents(playerLoginListener, this);
      pluginManager.registerEvents(playerJoinListener, this);
      pluginManager.registerEvents(playerDeathListener, this);
      pluginManager.registerEvents(playerDamageListener, this);
      pluginManager.registerEvents(playerHungerListener, this);
      pluginManager.registerEvents(playerPVPListener, this);
      pluginManager.registerEvents(playerHealthListener, this);
      pluginManager.registerEvents(playerQuitListener, this);
      pluginManager.registerEvents(blockBreakListener, this);
      pluginManager.registerEvents(playerChat, this);

      //register gui listeners
      pluginManager.registerEvents(whitelistGUI, this);
      pluginManager.registerEvents(whitelistRemovePlayersGUI, this);

  }

  private void registerCommands() {
        getCommand("uhc").setExecutor(new CommandManager());
        getServer().getPluginCommand("meetup").setExecutor(new MeetUpCommand());
        getServer().getPluginCommand("timeleft").setExecutor(new TimeLeftCommand());
        getServer().getPluginCommand("uhcreload").setExecutor(new UHCReloadCommand());
        getCommand("wl").setExecutor(new WhitelistCommand());
        getCommand("ping").setExecutor(new PingCommand());
  }


 }


