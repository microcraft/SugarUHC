package com.microcraftmc.uhc.timers;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class TimerManager {

    public static TimerManager instance = new TimerManager();
    private Map<String,UHCTimer> timers = new HashMap<String,UHCTimer>();
    private UHCTimer mainTimer = null;

    /**
     * Cached list of the running timers
     */
    private Map<String,UHCTimer> runningTimers = new HashMap<String,UHCTimer>();

    /**
     * List of the timers to resume if running timers are paused.
     *
     * @see {@link #pauseAllRunning(boolean)}.
     */
    private HashSet<UHCTimer> timersToResume = new HashSet<UHCTimer>();


    public TimerManager() {

    }

    public static TimerManager getInstance() {
        return instance;
    }

    /**
     * Registers the main timer, used to display the episodes countdown.
     *
     * @param timer The timer.
     */
    public void registerMainTimer(UHCTimer timer) {
        this.mainTimer = timer;
        timer.setRegistered(true);
    }

    /**
     * Returns the main timer, used to display the episodes countdown.
     *
     * @return The main timer.
     */
    public UHCTimer getMainTimer() {
        return this.mainTimer;
    }

    /**
     * Registers a timer.
     *
     * @param timer The timer to register.
     * @throws IllegalArgumentException if a timer with the same name is already registered.
     */
    public void registerTimer(UHCTimer timer) {

        if(timers.get(timer.getName()) != null) {
            throw new IllegalArgumentException("A timer with the name " + timer.getName() + " is already registered.");
        }

        timers.put(timer.getName(), timer);

        timer.setRegistered(true);
    }

    /**
     * Unregisters a timer.
     * <p>
     * If the timer was not registered, nothing is done.
     *
     * @param timer The timer to unregister.
     */
    public void unregisterTimer(UHCTimer timer) {
        timers.remove(timer.getName());
        runningTimers.remove(timer.getName());

        timer.setRegistered(false);
    }

    /**
     * Updates the internal list of started timers.
     */
    public void updateStartedTimersList() {
        runningTimers = new HashMap<String,UHCTimer>();

        if(getMainTimer() != null && getMainTimer().isRunning()) {
            runningTimers.put(getMainTimer().getName(), getMainTimer());
        }

        for(UHCTimer timer : timers.values()) {
            if(timer.isRunning()) {
                runningTimers.put(timer.getName(), timer);
            }
        }
    }

    /**
     * Returns a timer by his name.
     *
     * @param name The name of the timer.
     *
     * @return The timer, or null if there isn't any timer with this name.
     */
    public UHCTimer getTimer(String name) {
        return timers.get(name);
    }

    /**
     * Returns a collection containing the registered timers.
     *
     * @return The collection.
     */
    public Collection<UHCTimer> getTimers() {
        return timers.values();
    }

    /**
     * Returns a collection containing the running timers.
     *
     * @return The collection.
     */
    public Collection<UHCTimer> getRunningTimers() {
        return runningTimers.values();
    }

    /**
     * Pauses (or resumes) all the running timers.
     *
     * @param paused If true, all the timers will be paused. Else, resumed.
     */
    public void pauseAll(boolean paused) {
        for(UHCTimer timer : getRunningTimers()) {
            timer.setPaused(paused);
        }

        if(!paused) {
            // If we restart all the timers regardless to their previous state,
            // this data is meaningless.
            timersToResume.clear();
        }
    }

    /**
     * Pauses (or resumes) all the running timers.
     * <p>
     * This method will only resume the previously-running timers.
     *
     * @param paused If true, all the timers will be paused. Else, resumed.
     */
    public void pauseAllRunning(boolean paused) {
        if(paused) {
            for(UHCTimer timer : getRunningTimers()) {
                if(!timer.isPaused()) {
                    timer.setPaused(true);
                    timersToResume.add(timer);
                }
            }
        }
        else {
            for(UHCTimer timer : timersToResume) {
                timer.setPaused(false);
            }

            timersToResume.clear();
        }
    }

}
