package com.microcraftmc.uhc.integration;


import com.microcraftmc.uhc.Settings;
import com.microcraftmc.uhc.utils.LogUtil;
import com.wimbli.WorldBorder.BorderData;
import com.wimbli.WorldBorder.Config;
import com.wimbli.WorldBorder.WorldBorder;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.plugin.Plugin;

import javax.swing.border.Border;

public class WorldBorderIntegration {

    private static WorldBorderIntegration instance = new WorldBorderIntegration();
    private WorldBorder wb = null;

    public WorldBorderIntegration() {

    }

    public static WorldBorderIntegration getInstance() {
        return instance;
    }

    public void setup() {

        Plugin wbTest = Bukkit.getServer().getPluginManager().getPlugin("WorldBorder");
        if (wbTest == null || !wbTest.isEnabled()) {
            LogUtil.LogToConsole("WorldBorder not found, integration disabled.");
            return;
        }

        wb = (WorldBorder) wbTest;

        try {
            Class.forName("com.wimbli.WorldBorder.BorderData");
            Class.forName("com.wimbli.WorldBorder.Config");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //WorldBorder is available, so lets setup the borders for our world
        setupBorders();


        LogUtil.LogToConsole("Hooked into WorldBorder successfully!");
    }

    public void setupBorders() {

        if (!isWBIntegrationEnabled()) {
            return;
        }

        //General Configuration
        Config.setPortalRedirection(true); //Because the nether is border-less

        //Overworld border
        World overworld = getOverworld();
        BorderData borderOverworld = wb.getWorldBorder(overworld.getName());

        if(borderOverworld == null) {
            // The border needs to be created from scratch
            borderOverworld = new BorderData(0, 0, 0); // Random values, overwritten later.
        }


    }

    private World getOverworld() {
        /*for (World world : Bukkit.getServer().getWorlds()) {
            if(world.getEnvironment() != Environment.NETHER && world.getEnvironment() != Environment.THE_END) {
                return world;
            }
        }
        return null;*/
        return Bukkit.getWorld(Settings.WorldName);
    }

    private boolean isWBIntegrationEnabled() {
        return !(wb == null);
    }

    public WorldBorder getWorldBorder() {
        return wb;
    }



}
