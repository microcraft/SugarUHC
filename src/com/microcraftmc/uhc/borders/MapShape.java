package com.microcraftmc.uhc.borders;

import com.microcraftmc.uhc.borders.shapes.MapShapeDescriptor;
import com.microcraftmc.uhc.borders.shapes.SquaredMapShape;
import com.microcraftmc.uhc.borders.shapes.CircularMapShape;
import com.microcraftmc.uhc.borders.generators.WallGenerator;
import com.microcraftmc.uhc.borders.generators.SquaredWallGenerator;
import com.microcraftmc.uhc.borders.generators.CircularWallGenerator;

import com.microcraftmc.uhc.utils.LogUtil;
import org.bukkit.Material;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;

public enum MapShape {

    CIRCULAR(new CircularMapShape(), CircularWallGenerator.class),
    SQUARED(new SquaredMapShape(), SquaredWallGenerator.class);


    private MapShapeDescriptor shape;
    private Class<? extends WallGenerator> generatorClass;

    /**
     * @param generator The wall generator class associated with this shape.
     */
    private MapShape(MapShapeDescriptor shape, Class<? extends WallGenerator> generator) {
        this.shape          = shape;
        this.generatorClass = generator;
    }

    /**
     * Returns a new instance of the wall generator for this shape.
     *
     * @return The instance.
     */
    public WallGenerator getWallGeneratorInstance(Material wallBlockAir, Material wallBlockSolid) {

        try {
            Constructor constructor = generatorClass.getConstructor(Material.class, Material.class);
            return (WallGenerator) constructor.newInstance(wallBlockAir, wallBlockSolid);

        } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
            LogUtil.LogToConsole("Cannot instantiate the walls generator: invalid class.");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the shape descriptor.
     *
     * @return The shape.
     */
    public MapShapeDescriptor getShape() {
        return shape;
    }

    /**
     * Returns a shape based on his name.
     *
     * <p>Not case sensitive.</p>
     *
     * @param name The name.
     * @return The MapShape, or {@code null} if not found.
     */
    public static MapShape fromString(String name) {
        try {
            return MapShape.valueOf(name.trim().toUpperCase());
        } catch(IllegalArgumentException e) {
            return null;
        }
    }

}
