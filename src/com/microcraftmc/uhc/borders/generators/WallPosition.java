package com.microcraftmc.uhc.borders.generators;

public enum WallPosition {
    NORTH,
    SOUTH,
    EAST,
    WEST
}
