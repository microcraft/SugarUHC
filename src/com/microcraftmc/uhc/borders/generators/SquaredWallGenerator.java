package com.microcraftmc.uhc.borders.generators;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

public class SquaredWallGenerator extends WallGenerator {

    public SquaredWallGenerator(Material wallBlockAir, Material wallBlockSolid) {
        super(wallBlockAir, wallBlockSolid);
    }

    /**
     * Builds a wall in the world.
     *
     * @param world      The world the wall will be built in.
     * @param diameter   The diameter of the wall.
     * @param wallHeight The height of the wall.
     */
    @Override
    public void build(World world, int diameter, int wallHeight) {

        Integer halfDiameter = (int) Math.floor(diameter/2);

        Location spawn = world.getSpawnLocation();
        Integer limitXInf = spawn.add(-halfDiameter, 0, 0).getBlockX();

        spawn = world.getSpawnLocation();
        Integer limitXSup = spawn.add(halfDiameter, 0, 0).getBlockX();

        spawn = world.getSpawnLocation();
        Integer limitZInf = spawn.add(0, 0, -halfDiameter).getBlockZ();

        spawn = world.getSpawnLocation();
        Integer limitZSup = spawn.add(0, 0, halfDiameter).getBlockZ();

        for (Integer x = limitXInf; x <= limitXSup; x++) {
            world.getBlockAt(x, 1, limitZInf).setType(Material.BEDROCK);
            world.getBlockAt(x, 1, limitZSup).setType(Material.BEDROCK);

            for (Integer y = 2; y <= wallHeight; y++) {
                setBlock(world.getBlockAt(x, y, limitZInf), WallPosition.NORTH);
                setBlock(world.getBlockAt(x, y, limitZSup), WallPosition.SOUTH);
            }
        }

        for (Integer z = limitZInf + 1; z <= limitZSup - 1; z++) {
            world.getBlockAt(limitXInf, 1, z).setType(Material.BEDROCK);
            world.getBlockAt(limitXSup, 1, z).setType(Material.BEDROCK);

            for (Integer y = 2; y <= wallHeight; y++) {
                setBlock(world.getBlockAt(limitXInf, y, z), WallPosition.WEST);
                setBlock(world.getBlockAt(limitXSup, y, z), WallPosition.EAST);
            }
        }

    }

}
