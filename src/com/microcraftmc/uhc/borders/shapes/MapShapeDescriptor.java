package com.microcraftmc.uhc.borders.shapes;

import org.bukkit.Location;

public interface MapShapeDescriptor {


    /**
     * Returns true if the given location is inside the map.
     *
     * @param location The location to check.
     * @param diameter The diameter of the map.
     * @param center   The center of the map.
     *
     * @return {@code true} if the given location is inside the map.
     */
    public boolean isInsideBorder(final Location location, final Double diameter, final Location center);

    /**
     * Returns the distance between the given location and the border with this diameter.
     *
     * @param location The distance will be calculated between this location and the closest point of the border.
     * @param diameter The diameter of the border.
     * @param center   The center of the border.
     *
     * @return The distance between the given {@code location} and the closest point of the border.<br />
     *         {@code -1} if the location is inside the border.
     */
    public double getDistanceToBorder(final Location location, final Double diameter, final Location center);


}
