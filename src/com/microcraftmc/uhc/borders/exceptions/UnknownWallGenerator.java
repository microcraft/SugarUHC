package com.microcraftmc.uhc.borders.exceptions;

public class UnknownWallGenerator extends Exception {

    public UnknownWallGenerator(String message) {
        super(message);
    }

}
