package com.microcraftmc.uhc.borders.exceptions;


public class CannotGenerateWallsException extends Exception {

    public CannotGenerateWallsException(String message) {
        super(message);
    }

}
