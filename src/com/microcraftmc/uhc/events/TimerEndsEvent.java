package com.microcraftmc.uhc.events;

import com.microcraftmc.uhc.timers.UHCTimer;

public class TimerEndsEvent extends UHCEvent {

    private UHCTimer timer;
    private Boolean timerWasUp = false;
    private Boolean restart = false;


    public TimerEndsEvent(UHCTimer timer, Boolean timerUp) {
        this.timer = timer;

        this.timerWasUp = timerUp;
    }

    /**
     * Returns the timer.
     *
     * @return the timer.
     */
    public UHCTimer getTimer() {
        return timer;
    }

    /**
     * Returns true if the timer was stopped because it was up.
     *
     * @return true if the timer was stopped because it was up.
     */
    public boolean wasTimerUp() {
        return timerWasUp;
    }

    /**
     * If true, the timer will be restarted.
     *
     * @param restart true if the timer needs to be restarted.
     */
    public void setRestart(boolean restart) {
        this.restart = restart;
    }

    /**
     * Return true if the timer will be restarted.
     *
     * @param restart true if the timer will be restarted.
     * @return
     */
    public boolean getRestart() {
        return this.restart;
    }

}
