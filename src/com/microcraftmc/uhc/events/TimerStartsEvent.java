package com.microcraftmc.uhc.events;

import com.microcraftmc.uhc.timers.UHCTimer;

public class TimerStartsEvent extends UHCEvent {

    private UHCTimer timer;

    public TimerStartsEvent(UHCTimer timer) {
        this.timer = timer;
    }

    /**
     * Returns the timer.
     *
     * @return
     */
    public UHCTimer getTimer() {
        return timer;
    }

}
