package com.microcraftmc.uhc;

import java.util.ArrayList;
import java.util.UUID;
import org.bukkit.scoreboard.Scoreboard;



public class Settings
{
    public static Scoreboard Scoreboard;
    public static ArrayList<UUID> Players = new ArrayList();

    public static Integer MapRadius = Integer.valueOf(1000);
    public static Integer GameTime = Integer.valueOf(60);
    public static Integer PVPTime = Integer.valueOf(10);
    public static Integer MinPlayers = Integer.valueOf(2);
    public static Integer TimeLeft = Integer.valueOf(0);
    public static Integer GameTimer = 0;

    public static boolean GameInProgress = false;
    public static boolean GameInMeetup = false;
    public static boolean GameOver = false;
    public static boolean PVPEnabled = false;
    public static boolean SpectatingEnabled = false;
    public static boolean SpectatingJoin = false;
    public static boolean SpeedMode = false;
    public static boolean ShowHeartsInTab = false;
    public static boolean ShowHealthInTab = false;
    public static boolean ShowTimeInTab = false;
    public static boolean DebugMode = false;

    public static String WorldName = "UHC_World";
    public static String LobbyName = "uhclobby";
    public static String LobbyWorld = "UHC_World";
    public static Double LobbySpawnX = 0.0D;
    public static Double LobbySpawnZ = 0.0D;
    public static Double LobbySpawnY = 204.0D;

    public static String Title = "Title";
    public static String Subtitle = "Subtitle";
    public static String TabTitle = "Tab Title";
    public static String TabFooter = "Tab Footer";
    public static Integer titleFadeInTime = 60;
    public static Integer titleFadeOutTime = 60;
    public static Integer titleStayTime = 60;


}


