package com.microcraftmc.uhc.tasks;

import com.microcraftmc.uhc.Settings;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.config.MessageManager;
import com.microcraftmc.uhc.scoreboard.GameScoreboardManager;
import com.microcraftmc.uhc.utils.TimeUtil;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Iterator;
import java.util.List;

public class GameTimerTask extends BukkitRunnable {

    private static GameTimerTask instance = new GameTimerTask();
    private List<String> rulesList = ConfigManager.getInstance().getRulesConfig().getStringList("default-msgs.rules");
    Iterator rulesIterator = rulesList.iterator();
    boolean taskRunning;
    int count = 0;

    public GameTimerTask() {

    }

    public static GameTimerTask getInstance() {
        return instance;
    }


    public void run() {

        taskRunning = true;

        try {

            if (count < 3900) {
                Settings.GameTimer = Settings.GameTimer + 1;
                GameScoreboardManager.getInstance().updateScoreboardGame();
                count++;
            } else {
                taskRunning = false;
                this.cancel();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
