package com.microcraftmc.uhc.tasks;

import com.microcraftmc.uhc.Settings;
import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.config.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Iterator;
import java.util.List;

public class PVPTimerTask extends BukkitRunnable {

    private static PVPTimerTask instance = new PVPTimerTask();
    private List<String> rulesList = ConfigManager.getInstance().getRulesConfig().getStringList("default-msgs.rules");
    Iterator rulesIterator = rulesList.iterator();
    boolean countdownRunning;
    int count = Settings.PVPTime * 60;

    public PVPTimerTask() {

    }

    public static PVPTimerTask getInstance() {
        return instance;
    }


    public void run() {

        countdownRunning = true;

        try {


            if (Settings.PVPEnabled) {
                return;
            }


            if (count > 0) {
                if (count <= 10) {
                    MessageManager.getInstance().broadcastMessage(MessageManager.PrefixType.INFO, ChatColor.GOLD + "PVP will be enabled in " + count + " seconds");
                }
                count--;
            } else {
                MessageManager.getInstance().broadcastMessage(MessageManager.PrefixType.INFO, ChatColor.GOLD + "PVP has been enabled");
                Settings.PVPEnabled = true;
                countdownRunning = false;
                this.cancel();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
