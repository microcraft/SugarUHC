package com.microcraftmc.uhc.tasks;

import com.microcraftmc.uhc.Settings;
import com.microcraftmc.uhc.borders.BorderManager;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.config.MessageManager;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import com.microcraftmc.uhc.borders.MapShape;

import java.util.Iterator;
import java.util.List;

public class BorderWarningTask extends BukkitRunnable {

    private static BorderWarningTask instance = new BorderWarningTask();
    private List<String> rulesList = ConfigManager.getInstance().getRulesConfig().getStringList("default-msgs.rules");
    Iterator rulesIterator = rulesList.iterator();
    boolean taskRunning;

    public BorderWarningTask() {

    }

    public static BorderWarningTask getInstance() {
        return instance;
    }


    public void run() {

        taskRunning = true;

        try {


            // Message sent to all players outside the border
            for(Player player : BorderManager.getInstance().getPlayersOutside(BorderManager.getInstance().getWarningSize())) {
                double distance = BorderManager.getInstance().getDistanceToBorder(player.getLocation(), BorderManager.getInstance().getWarningSize());

                if(BorderManager.getInstance().getMapShape() == MapShape.CIRCULAR) {
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.WARNING, String.format("The border will be shrinking to %s, if you are outside the" +
                            " border, you will be tped when the border shrink happens.", String.valueOf(BorderManager.getInstance().getWarningSize())), player);
                } else {
                    MessageManager.getInstance().sendMessage(MessageManager.PrefixType.WARNING, String.format("The border will be shrinking to %s, if you are outside the" +
                            " border, you will be tped when the border shrink happens.", String.valueOf(BorderManager.getInstance().getWarningSize())), player);
                }

            }

            taskRunning = false;
            this.cancel();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
