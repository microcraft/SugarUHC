package com.microcraftmc.uhc.tasks;

import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.config.MessageManager;
import com.microcraftmc.uhc.scoreboard.LobbyScoreboardManager;

import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

public class ScoreboardCountdownTask extends BukkitRunnable {

    private static ScoreboardCountdownTask instance = new ScoreboardCountdownTask();
    private boolean countdownRunning;
    int count = 60;

    public ScoreboardCountdownTask() {

    }

    public static ScoreboardCountdownTask getInstance() {
        return instance;
    }

    public void run() {

        countdownRunning = true;

        if (count > 0) {
            if (count < 10) {
                for (Player players : Bukkit.getServer().getOnlinePlayers()) {
                    players.playSound(players.getLocation(), Sound.ORB_PICKUP, 1, 0);
                }
            }
            LobbyScoreboardManager.getInstance().updateScoreboardLobbyWithTime(ChatColor.WHITE + "Starting In " + ChatColor.GREEN + count + " seconds");
            count--;
        } else {
            LobbyScoreboardManager.getInstance().hideScoreboardLobby();
            MessageManager.getInstance().broadcastMessage(MessageManager.PrefixType.INFO, ConfigManager.getInstance().getRulesConfig().getString("default-msgs.rules-start"));
            RulesListTask.getInstance().runTaskTimer(SugarUHC.getPlugin(), 0, 10);
            countdownRunning = false;
            this.cancel();
        }


    }

}
