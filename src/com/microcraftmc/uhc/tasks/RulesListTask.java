package com.microcraftmc.uhc.tasks;

import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.config.MessageManager;
import com.microcraftmc.uhc.scoreboard.LobbyScoreboardManager;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;


import java.util.List;

public class RulesListTask extends BukkitRunnable {

    private static RulesListTask instance = new RulesListTask();
    private List<String> rulesList = ConfigManager.getInstance().getRulesConfig().getStringList("default-msgs.rules");
    Iterator rulesIterator = rulesList.iterator();
    boolean countdownRunning;

    public RulesListTask() {

    }

    public static RulesListTask getInstance() {
        return instance;
    }


    public void run() {

        countdownRunning = true;

        try {

            if (rulesIterator.hasNext()) {
                MessageManager.getInstance().broadcastMessage(MessageManager.PrefixType.INFO, rulesIterator.next().toString());
                Thread.sleep(2000);
            } else {
                StartGameTask.getInstance().runTaskTimer(SugarUHC.getPlugin(), 0, 20);
                countdownRunning = false;
                this.cancel();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
