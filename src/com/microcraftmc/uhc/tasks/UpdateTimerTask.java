package com.microcraftmc.uhc.tasks;

import com.microcraftmc.uhc.Settings;
import com.microcraftmc.uhc.borders.BorderManager;
import com.microcraftmc.uhc.borders.MapShape;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.config.MessageManager;
import com.microcraftmc.uhc.timers.TimerManager;
import com.microcraftmc.uhc.timers.UHCTimer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Iterator;
import java.util.List;

public class UpdateTimerTask extends BukkitRunnable {

    private static UpdateTimerTask instance = new UpdateTimerTask();
    boolean taskRunning;


    public UpdateTimerTask() {

    }

    public static UpdateTimerTask getInstance() {
        return instance;
    }


    public void run() {

        taskRunning = true;

        try {

            for(UHCTimer timer : TimerManager.getInstance().getRunningTimers()) {
                timer.update();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
