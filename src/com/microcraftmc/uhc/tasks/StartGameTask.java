package com.microcraftmc.uhc.tasks;

import com.microcraftmc.uhc.Settings;
import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.config.MessageManager;
import com.microcraftmc.uhc.handlers.PlayerHandler;
import com.microcraftmc.uhc.scoreboard.GameScoreboardManager;
import com.microcraftmc.uhc.utils.ScatterUtil;
import com.microcraftmc.uhc.utils.WorldUtil;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;
import java.util.List;

public class StartGameTask extends BukkitRunnable {

    private static StartGameTask instance = new StartGameTask();
    boolean taskRunning;

    public StartGameTask() {

    }

    public static StartGameTask getInstance() {
        return instance;
    }

    public void run() {

        taskRunning = true;

        try {

            for (Player player : PlayerHandler.getInstance().getPlayers()) {


                for (PotionEffect effect : player.getActivePotionEffects()) {
                    player.removePotionEffect(effect.getType());
                }


                PotionEffect potionEffect = new PotionEffect(PotionEffectType.BLINDNESS, 99999, 1);
                player.addPotionEffect(potionEffect);
                potionEffect = new PotionEffect(PotionEffectType.SLOW, 99999, 10);
                player.addPotionEffect(potionEffect);
                potionEffect = new PotionEffect(PotionEffectType.JUMP, 99999, 200);
                player.addPotionEffect(potionEffect);


                player.setHealth(20.0D);
                player.setFoodLevel(20);


                player.getInventory().clear();
                player.getInventory().setArmorContents(null);
                player.getInventory().setHelmet(new ItemStack(Material.AIR));
                player.getInventory().setChestplate(new ItemStack(Material.AIR));
                player.getInventory().setLeggings(new ItemStack(Material.AIR));
                player.getInventory().setBoots(new ItemStack(Material.AIR));


                player.setExp(0.0F);
                player.setLevel(0);
                player.setFireTicks(0);
                GameScoreboardManager.getInstance().setupScoreboardGame(player);

            }

            World world = Bukkit.getWorld(Settings.WorldName);
            world.setDifficulty(Difficulty.HARD);
            WorldBorder worldBorder = Bukkit.getWorld(Settings.WorldName).getWorldBorder();
            worldBorder.setCenter(0.0D, 0.0D);
            worldBorder.setSize(Settings.MapRadius * 2);

            Bukkit.broadcastMessage(ChatColor.GOLD + "Scattering players...");

            Integer i = 0;
            Integer localInteger1;
            Integer localInteger2;
            for (Iterator i$ = PlayerHandler.getInstance().getPlayers().iterator(); i$.hasNext();
                 localInteger2 = i = i + 1)
            {
                final Player player = (Player)i$.next();
                BukkitRunnable scatter = new BukkitRunnable()
                {
                    public void run() {
                        ScatterUtil.scatterPlayerRandom(player, Settings.MapRadius);
                    }

                };
                scatter.runTaskLater(SugarUHC.getPlugin(), i * 2L);
                localInteger1 = i;
            }

            i = i + 5;
            BukkitRunnable finalRunnable = new BukkitRunnable()
            {
                public void run() {
                    for (Iterator i$ = PlayerHandler.getInstance().getPlayers().iterator(); i$.hasNext();) { Player player = (Player)i$.next();
                        for (PotionEffect effect : player.getActivePotionEffects())
                            player.removePotionEffect(effect.getType());
                    }
                    Player player;
                    World world = Bukkit.getWorld(Settings.WorldName);
                    if (world != null) world.setFullTime(0L);
                    Bukkit.broadcastMessage(ChatColor.GOLD + "Game has started");
                    Settings.GameInProgress = true;
                    if (Settings.PVPTime > 0) {
                        Bukkit.broadcastMessage(ChatColor.GOLD + "PVP will be enabled in " + Settings.PVPTime + " minutes");
                        //GameHandler.StartPVPCountdown();
                        GameTimerTask.getInstance().runTaskTimer(SugarUHC.getPlugin(), 0, 20);
                        PVPTimerTask.getInstance().runTaskTimer(SugarUHC.getPlugin(), 0, 20);
                    } else {
                        Settings.PVPEnabled = true;
                    }
                    //GameHandler.StartMeetUpCountdown();
                }

            };
            finalRunnable.runTaskLater(SugarUHC.getPlugin(), i * 3L);
            WorldUtil.clearSpawn(Settings.WorldName);
            //GameScoreboardManager.getInstance().updateScoreboardGame();

            taskRunning = false;
            this.cancel();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
