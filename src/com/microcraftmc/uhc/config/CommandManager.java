package com.microcraftmc.uhc.config;

import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.commands.*;
import com.microcraftmc.uhc.config.MessageManager.PrefixType;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

public class CommandManager implements CommandExecutor {

    private static Plugin p = SugarUHC.getPlugin();
    private static CommandManager instance = new CommandManager();
    private HashMap <String,SubCommand> commands;
    private HashMap <String,Integer> helpinfo;
    private static MessageManager messageManager = MessageManager.getInstance();
    private PluginDescriptionFile pdFile = p.getDescription();

    public CommandManager() {
        setup();
    }

    public static CommandManager getInstance() {
        return instance;
    }

    public void setup() {
        commands = new HashMap<String,SubCommand>();
        helpinfo = new HashMap<String,Integer>();
        loadCommands();
        loadHelpInfo();
    }

    private void loadCommands() {
        commands.put("start", new StartCommand());
        commands.put("debug", new DebugCommand());
        commands.put("setlobby", new SetLobbyCommand());
        commands.put("mod", new UHCModCommand());
        commands.put("host", new UHCHostCommand());
        commands.put("config", new ConfigCommand());
    }

    private void loadHelpInfo() {
        helpinfo.put("start", 2);
        helpinfo.put("debug", 3);
        helpinfo.put("setlobby", 3);
        helpinfo.put("mod", 2);
        helpinfo.put("host", 2);
        helpinfo.put("config", 2);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (cmd.getName().equalsIgnoreCase("uhc")) {

                if (args == null || args.length < 1) {
                    messageManager.sendMessage(PrefixType.INFO, "SugarUHC Version " + pdFile.getVersion(), player);
                    messageManager.sendMessage(PrefixType.INFO, "Type /uhc help <player | staff | admin> for command information", player);
                    return true;
                }

                if (args[0].equalsIgnoreCase("help")) {
                    if (args[1].equalsIgnoreCase("player")) {
                        help(player, 1);
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("staff")) {
                        help(player, 2);
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("admin")) {
                        help(player, 3);
                        return true;
                    }
                    else {
                        messageManager.sendMessage(PrefixType.WARNING, args[1] + " is not a valid page! Valid pages are Player, Staff, and Admin.", player);
                    }

                }

                /*for (String command: commands.keySet()) {
                    if (command.contains(args[0])) {
                        commands.get(command).onCommand(player, args);
                    }
                    else
                    {
                        messageManager.sendMessage(PrefixType.ERROR, "Command doesn't exist", player);
                        messageManager.sendMessage(PrefixType.INFO, "Type /uhc help <player | staff | admin> for command information", player);
                    }
                }*/

                String sub = args[0];
                Vector<String> l = new Vector<String>();
                l.addAll(Arrays.asList(args));
                l.remove(0);
                args = (String[]) l.toArray(new String[0]);
                if (!commands.containsKey(sub)) {
                    messageManager.sendMessage(PrefixType.ERROR, "Command doesn't exist", player);
                    messageManager.sendMessage(PrefixType.INFO, "Type /uhc help <player | staff | admin> for command information", player);
                    return true;
                }

                try {
                    commands.get(sub).onCommand(player, args);
                } catch (Exception e) {
                    e.printStackTrace();
                    messageManager.sendFMessage(PrefixType.ERROR, "error.command", player, "command-["+sub+"] "+Arrays.toString(args));
                    messageManager.sendMessage(PrefixType.INFO, "Type /uhc help <player } staff | admin> for command information", player);
                }
                return true;
            }
        }

        return false;

    }

    public void help(Player p, int page) {
        if (page == 1) {
            p.sendMessage(ChatColor.BLUE + "------------ " + PrefixType.MAIN + ChatColor.DARK_AQUA + " Player Commands" + ChatColor.BLUE + " ------------");
        }
        if (page == 2) {
            p.sendMessage(ChatColor.BLUE + "------------ " + PrefixType.MAIN + ChatColor.DARK_AQUA + " Staff Commands" + ChatColor.BLUE + " ------------");
        }
        if (page == 3) {
            p.sendMessage(ChatColor.BLUE + "------------ " + PrefixType.MAIN + ChatColor.DARK_AQUA + " Admin Commands" + ChatColor.BLUE + " ------------");
        }

        for (String command : commands.keySet()) {
            try {
                if (helpinfo.get(command) == page) {
                    messageManager.sendMessage(PrefixType.INFO, commands.get(command).help(p), p);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



}
