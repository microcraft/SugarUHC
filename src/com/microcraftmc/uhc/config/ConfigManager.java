package com.microcraftmc.uhc.config;

import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.utils.LogUtil;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.*;


public class ConfigManager {

    private static ConfigManager instance = new ConfigManager();
    private static Plugin p = SugarUHC.getPlugin();

    private FileConfiguration lobby;
    private FileConfiguration messages;
    private FileConfiguration game;
    private FileConfiguration rules;
    private FileConfiguration world;
    private FileConfiguration scoreboards;

    private File lobbyFile;
    private File messagesFile;
    private File gameFile;
    private File rulesFile;
    private File worldFile;
    private File scoreboardsFile;


    public ConfigManager() {

    }

    public static ConfigManager getInstance() {
        return instance;
    }

    public void setup() {

        //load the default config
        p.getConfig().options().copyDefaults(true);
        p.saveDefaultConfig();

        //load our config files
        lobbyFile = new File(p.getDataFolder(), "lobby.yml");
        messagesFile = new File(p.getDataFolder(), "messages.yml");
        gameFile = new File(p.getDataFolder(), "game.yml");
        rulesFile = new File(p.getDataFolder(), "rules.yml");
        worldFile = new File(p.getDataFolder(), "world.yml");
        scoreboardsFile = new File(p.getDataFolder(), "scoreboards.yml");

        try {
            if(!lobbyFile.exists()) {
                loadFile("lobby.yml");
            }
            if(!messagesFile.exists()) {
                loadFile("messages.yml");
            }
            if(!gameFile.exists()) {
                loadFile("game.yml");
            }
            if(!rulesFile.exists()) {
                loadFile("rules.yml");
            }
            if(!worldFile.exists()) {
                loadFile("world.yml");
            }
            if (!scoreboardsFile.exists()) {
                loadFile("scoreboards.yml");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //load our config files
        lobby = YamlConfiguration.loadConfiguration(lobbyFile);
        messages = YamlConfiguration.loadConfiguration(messagesFile);
        game = YamlConfiguration.loadConfiguration(gameFile);
        rules = YamlConfiguration.loadConfiguration(rulesFile);
        world = YamlConfiguration.loadConfiguration(worldFile);
        scoreboards = YamlConfiguration.loadConfiguration(scoreboardsFile);

    }


    public FileConfiguration getConfig() {
        return p.getConfig();
    }

    public FileConfiguration getLobbyConfig() {
        return lobby;
    }

    public FileConfiguration getMessagesConfig() {
        return messages;
    }

    public FileConfiguration getGameConfig() {
        return game;
    }

    public FileConfiguration getRulesConfig() {
        return rules;
    }

    public FileConfiguration getWorldConfig() {
        return world;
    }

    public FileConfiguration getScoreboardsConfig() {
        return scoreboards;
    }

    public boolean isDebugEnabled() {
        return p.getConfig().getBoolean("debug");
    }

    public void loadFile(String file)
    {
        File t = new File(p.getDataFolder(), file);
        LogUtil.LogToConsole("Writing new file: " + t.getAbsolutePath());

        try {
            t.createNewFile();
            FileWriter out = new FileWriter(t);
            if (!isDebugEnabled()) {
                //should we print the file we are writing for debug purposes?
                System.out.println(file);
            }
            //InputStream is = getClass().getResourceAsStream("/"+file);
            InputStream is = p.getResource(file);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                out.write(line + "\n");
                if (!isDebugEnabled()) {
                    System.out.print(line);
                }
            }
            out.flush();
            is.close();
            isr.close();
            br.close();
            out.close();

            if (!isDebugEnabled()) {
                LogUtil.LogToConsole("Loaded Config " + file + " successfully!");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveConfig() {
        p.saveConfig();
    }

    public void saveLobbyConfig() {
        try {

            //save our lobby config file
            lobby.save(lobbyFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveMessagesConfig() {
        try {

            //save our messages config file
            messages.save(messagesFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveGameConfig() {
        try {

            //save our game config file
            game.save(gameFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveRulesConfig() {
        try {

            //save our rules config file
            rules.save(rulesFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveWorldConfig() {
        try {
            //save our world config file
            world.save(worldFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveScoreboardsConfig() {
        try {
            //save our scoreboards config file
            scoreboards.save(scoreboardsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
