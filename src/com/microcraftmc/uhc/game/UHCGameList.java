package com.microcraftmc.uhc.game;

import java.util.ArrayList;
import com.microcraftmc.uhc.exceptions.GameIDNotFoundException;

public class UHCGameList extends ArrayList<UHCGame> {

    private int currentGame = -1;

    public UHCGame getCurrentGame()
    {
        if (this.currentGame > -1) {
            return (UHCGame)get(this.currentGame);
        }
        return null;
    }

    public void setCurrentGame(String paramString) throws GameIDNotFoundException
    {
        for (int i = 0; i < size(); i++) {
            if (((UHCGame)get(i)).getGameID().equals(paramString)) {
                this.currentGame = i;
            }
        }
        throw new GameIDNotFoundException();
    }

}
