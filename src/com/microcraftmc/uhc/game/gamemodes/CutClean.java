package com.microcraftmc.uhc.game.gamemodes;

import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.game.UHCGame;
import com.microcraftmc.uhc.game.gamemodes.listeners.CutCleanListener;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CutClean extends UHCGame {

    public static final String GAME_ID = "CutClean";
    private static final String GAME_NAME = "CutClean";

    public CutClean()
    {
        super("CutClean", "CutClean");
    }

    public void onGameInit() {

        //register our CutClean listener
        Bukkit.getServer().getPluginManager().registerEvents(new CutCleanListener(), SugarUHC.getPlugin());

    }

    public void onGameStart() {
        ItemStack steak = new ItemStack(Material.COOKED_BEEF, 10);

        for (Player p : Bukkit.getOnlinePlayers()) {
            p.getInventory().addItem(steak);
        }
    }

}
