package com.microcraftmc.uhc.game.gamemodes;

import com.microcraftmc.uhc.game.UHCGame;

public class Vanilla extends UHCGame {

    public static final String GAME_ID = "Vanilla";
    private static final String GAME_NAME = "Vanilla";

    public Vanilla()
    {
        super("Vanilla", "Vanilla");
    }


}
