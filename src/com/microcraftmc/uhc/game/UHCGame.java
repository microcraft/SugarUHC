package com.microcraftmc.uhc.game;

import com.microcraftmc.uhc.exceptions.GameNotRunningException;
import org.bukkit.event.Listener;


public abstract class UHCGame implements Listener {

    private String GAME_ID = "";
    private String GAME_NAME = "";
    private boolean currentlyActive = false;
    private boolean TeamMode = false;

    public final boolean equals(Object paramObject)
    {
        return ((paramObject instanceof UHCGame)) && (((UHCGame)paramObject).getGameID().equals(this.GAME_ID));
    }

    protected UHCGame(String gameID, String gameName)
    {
        setGameID(gameID);
        setGameName(gameName);
    }

    public final String getGameID()
    {
        return this.GAME_ID;
    }

    protected final void setGameID(String paramString)
    {
        this.GAME_ID = paramString;
    }

    public final String getGameName()
    {
        return this.GAME_NAME;
    }

    protected final void setGameName(String paramString)
    {
        this.GAME_NAME = paramString;
    }

    public boolean canGameStart()
    {
        return true;
    }

    public void onGameInit() {}

    public void onGameLoad() {}

    public void onGameStart() {}

    public void onGameStop() {}

    public void onGameUnload() {}

    protected final void finishGame() {}

    public final boolean isCurrentlyActive()
    {
        return this.currentlyActive;
    }

    protected final void setCurrentlyActive(boolean paramBoolean)
    {
        this.currentlyActive = paramBoolean;
    }

    public final boolean isTeamMode() {
        return this.TeamMode;
    }

    protected  final void setTeamMode(boolean paramBoolean) {
        this.TeamMode = paramBoolean;
    }

}
