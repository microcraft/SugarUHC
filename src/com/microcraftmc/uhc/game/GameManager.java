package com.microcraftmc.uhc.game;


import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.exceptions.GameIDNotFoundException;
import com.microcraftmc.uhc.exceptions.GameNotRunningException;
import com.microcraftmc.uhc.exceptions.GameIDConflictException;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class GameManager {

    private static UHCGameList UHCGames = new UHCGameList();
    private static boolean gameInProgress = false;

    public static void addGameType(UHCGame paramUHCGame)
            throws GameIDConflictException
    {
        String str = paramUHCGame.getGameID();
        Iterator localIterator = UHCGames.iterator();
        while (localIterator.hasNext())
        {
            UHCGame localUHCGame = (UHCGame)localIterator.next();
            if (localUHCGame.getGameID().equals(str)) {
                throw new GameIDConflictException();
            }
        }
        UHCGames.add(paramUHCGame);
        Bukkit.getPluginManager().registerEvents(paramUHCGame, SugarUHC.getPlugin());
    }

    public void setActiveGame(String paramString)
            throws GameIDNotFoundException
    {
        Iterator localIterator = UHCGames.iterator();
        while (localIterator.hasNext())
        {
            UHCGame localUHCGame1 = (UHCGame)localIterator.next();
            if (localUHCGame1.getGameID().equals(paramString))
            {
                UHCGame localUHCGame2 = UHCGames.getCurrentGame();
                localUHCGame2.onGameStop();
                localUHCGame2.setCurrentlyActive(false);
                localUHCGame2.onGameUnload();
                UHCGames.setCurrentGame(localUHCGame1.getGameID());
                localUHCGame1.setCurrentlyActive(true);
                localUHCGame1.onGameLoad();
            }
        }
        throw new GameIDNotFoundException();
    }

    public static void finishCurrentGame() throws GameNotRunningException
    {
        if (gameInProgress)
        {
            UHCGames.getCurrentGame().onGameStop();
            gameInProgress = false;
        }
        else
        {
            throw new GameNotRunningException();
        }
    }

}
