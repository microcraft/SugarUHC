package com.microcraftmc.uhc.features;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.microcraftmc.uhc.SugarUHC;
import com.microcraftmc.uhc.config.ConfigManager;
import com.microcraftmc.uhc.features.core.NetherFeature;
import com.microcraftmc.uhc.utils.LogUtil;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import com.microcraftmc.uhc.exceptions.FeatureIDConflictException;
import com.microcraftmc.uhc.exceptions.FeatureIDNotFoundException;
import com.microcraftmc.uhc.exceptions.InvalidFeatureIDException;

public class FeatureManager {

    private static FeatureManager instance = new FeatureManager();
    private static UHCFeatureList features = new UHCFeatureList();
    private static final Pattern name_pattern = Pattern.compile("^[\\w]++$");

    public FeatureManager() {

    }

    public static FeatureManager getInstance() {
        return instance;
    }

    public void setup() {
        ArrayList localArrayList = new ArrayList();

        localArrayList.add(new NetherFeature(ConfigManager.getInstance().getGameConfig().getBoolean("Nether")));

        Iterator localIterator = localArrayList.iterator();
        while (localIterator.hasNext())
        {
            UHCFeature localUHCFeature = (UHCFeature)localIterator.next();
            try
            {
                addFeature(localUHCFeature);
                LogUtil.LogToConsole("Loaded feature module: " + localUHCFeature.getFeatureID());
            } catch (Exception localException) {
                LogUtil.LogToConsole("Failed to load a module " + (localUHCFeature == null ? "null" : localUHCFeature.getFeatureID()));
                localException.printStackTrace();
            }
        }
    }

    public void addFeature(UHCFeature paramUHCFeature) throws FeatureIDConflictException, InvalidFeatureIDException
    {
        Matcher localMatcher = name_pattern.matcher(paramUHCFeature.getFeatureID());
        if (!localMatcher.matches()) {
            throw new InvalidFeatureIDException();
        }
        Iterator localIterator = features.iterator();
        while (localIterator.hasNext())
        {
            UHCFeature localUHCFeature = (UHCFeature)localIterator.next();
            if (localUHCFeature.getFeatureID().equals(paramUHCFeature.getFeatureID())) {
                throw new FeatureIDConflictException();
            }
        }

        features.add(paramUHCFeature);

        if (paramUHCFeature.isEnabled()) {
            paramUHCFeature.enableFeature();
        } else {
            paramUHCFeature.disableFeature();
        }

        Bukkit.getPluginManager().registerEvents(paramUHCFeature, SugarUHC.getPlugin());
    }

    public boolean isEnabled(String paramString) throws FeatureIDNotFoundException
    {
        Iterator localIterator = features.iterator();

        while (localIterator.hasNext())
        {
            UHCFeature localUHCFeature = (UHCFeature)localIterator.next();
            if (localUHCFeature.getFeatureID().equals(paramString)) {
                return localUHCFeature.isEnabled();
            }
        }
        throw new FeatureIDNotFoundException();
    }

    public UHCFeature getFeature(String paramString) throws FeatureIDNotFoundException
    {
        Iterator localIterator = features.iterator();

        while (localIterator.hasNext())
        {
            UHCFeature localUHCFeature = (UHCFeature)localIterator.next();
            if (localUHCFeature.getFeatureID().equals(paramString)) {
                return localUHCFeature;
            }
        }
        throw new FeatureIDNotFoundException();
    }

    public UHCFeatureList getFeatures()
    {
        return features;
    }

    public List<String> getFeatureNames()
    {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = features.iterator();

        while (localIterator.hasNext())
        {
            UHCFeature localUHCFeature = (UHCFeature)localIterator.next();
            localArrayList.add(localUHCFeature.getFeatureID());
        }
        return localArrayList;
    }

}
