package com.microcraftmc.uhc.features;

import com.microcraftmc.uhc.exceptions.FeatureStateNotChangedException;
import org.bukkit.event.Listener;

public abstract class UHCFeature implements Listener {

    protected String featureID = "INVALID";
    private boolean enabled = false;
    private String description = "N/A";

    public abstract void enableFeature();

    public abstract void disableFeature();

    protected final void setFeatureID(String paramString)
    {
        this.featureID = paramString;
    }

    public final String getFeatureID()
    {
        return this.featureID;
    }

    public final boolean isEnabled()
    {
        return this.enabled;
    }

    public final void setEnabled(boolean paramBoolean) throws FeatureStateNotChangedException
    {
        if (paramBoolean != isEnabled())
        {
            if (paramBoolean)
            {
                this.enabled = true;
                enableFeature();
            }
            else
            {
                this.enabled = false;
                disableFeature();
            }
        }
        else {
            throw new FeatureStateNotChangedException();
        }
    }

    public UHCFeature(boolean paramBoolean)
    {
        this.enabled = paramBoolean;
    }

    public boolean equals(Object paramObject)
    {
        return ((paramObject instanceof UHCFeature)) && (((UHCFeature)paramObject).getFeatureID().equals(getFeatureID()));
    }

    public final String getDescription()
    {
        return this.description;
    }

    public final void setDescription(String paramString)
    {
        this.description = paramString;
    }

}
