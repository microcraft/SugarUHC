package com.microcraftmc.uhc.features.core;

import com.microcraftmc.uhc.config.MessageManager;
import com.microcraftmc.uhc.features.UHCFeature;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityPortalEvent;

public class NetherFeature extends UHCFeature {

    public NetherFeature(boolean paramBoolean)
    {
        super(paramBoolean);
        setFeatureID("DisableNether");
        setDescription("Disables the use of nether portals");
    }

    @EventHandler
    public void onPortalEvent(EntityPortalEvent e)
    {
        if ((isEnabled()) && (e.getTo().getWorld().getEnvironment() == World.Environment.NETHER)) {
            e.setCancelled(true);
        }
    }

    public void enableFeature() {}

    public void disableFeature() {}

}
